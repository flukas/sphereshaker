﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace SphereShaker.Utility
{
    class Log
    {
        /// <summary>
        /// Zapíše do logu záznam 
        /// </summary>
        /// <param name="message">Text záznamu</param>
        /// <param name="level">Hodnota záznamu</param>
        public void Write(string message, LogLevel level)
        {
            //Trace.Listeners.Add(new TextWriterTraceListener("c:\\temp\\SphereLog\\SphereLog.txt"));
            //Trace.AutoFlush = true;
            //log vypnut
            //System.Diagnostics.Trace.WriteLine(message);
                
                //TODO: Make it somehow work with the other log. Consider Events.
            //if (level == LogLevel.error)
            //    Utility.Log(message, LogMode.Error);
            //else
            //    Utility.Log(message, LogMode.LowLevel);
            //return;
        }

        public LogLevel Level { get; set; }

        //private List<LogEntry> entryList;

        private class LogEntry
        {
            public LogEntry(DateTime time, LogLevel level, string message, string threadName)
            {
                this.Time = time;
                this.Level = level;
                this.Message = message;
                this.ThreadName = threadName;
            }
            public LogEntry(LogLevel level, string message)
                : this(DateTime.Now, level, message, System.Threading.Thread.CurrentThread.Name)
            { }
            public DateTime Time { get; set; }
            public LogLevel Level { get; set; }
            public string Message { get; set; }
            public string ThreadName { get; set; }

            public override string ToString()
            {
                return Time.ToString() + "\t" + ThreadName + "\t" + Level.ToString() + "\t" + Message;
            }
        }

    }

    /// <summary>
    /// Zapíše do logu záznam 
    /// </summary>
    /// <param name="message">Text záznamu</param>
    /// <param name="level">Hodnota záznamu</param>
    public delegate void Logger(string message, LogLevel level);
    /// <summary>
    /// Úrovně pro záznam
    /// </summary>
    public enum LogLevel
    //CRITICAL - for critical errors
    //ERROR - for regular errors
    //WARNING - for warning messages
    //INFO - for informational messages
    //DEBUG - for debugging messages	
    {
        critical, error, warning, info, debug

    }

}
