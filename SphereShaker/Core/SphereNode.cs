﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace SphereShaker.Core
{
    //TODO: inherit from Sphere
    public class SphereNode : Sphere
    {
        public SphereNode(SphereFraction fraction)
            :base(fraction)
        {
            index = count;
            count++;

            neighbours = new List<SphereNode>(5);
            children = new List<SphereNode>();
            //Color = Color.Black;
            generation = 0;
        }
        public SphereNode(SphereFraction fraction, double x, double y)
        :base(fraction,x,y)
        {
            generation = 0;
            index = count;
            count++;

            Fraction = fraction;
            X = x;
            Y = y;
            neighbours = new List<SphereNode>(5);
            children = new List<SphereNode>();
            //Color = Color.Black;
        }

        public ulong index;
        public uint generation;
        private static ulong count = 0;
        public SphereNode Parent;
        public int ChildrenCount
        {
            get
            {
                return children.Count;
            }
        }
        public void AddChild(SphereNode child)
        {
            //todo: add youngest child if they touch
            if (!Touch(child))
            {
                throw new Exception("Child does not touch parent");
            }
            child.generation = this.generation + 1;
            child.Parent = this;
            child.neighbours.Add(this);
            //todo: přidat všechny potomky?
            if (this.LastChild != null)
            {
                child.neighbours.Add(this.LastChild);
                this.LastChild.neighbours.Add(child);
            }
            //if(children.Count>1 && Touch(this.children[0],child))
            //{
            //    child.neighbours.Add(children[0]);
            //    children[0].neighbours.Add(child);
            //}
            foreach (var item in this.neighbours)
            {
                if (Touch(child, item))
                {
                    child.neighbours.Add(item);
                    item.neighbours.Add(child);
                }
            }

            children.Add(child);
            neighbours.Add(child);


        }

        public SphereNode LastChild
        {
            get
            {
                if (children.Count == 0)
                    return null;
                else
                    return children.Last<SphereNode>();
            }
        }

        public SphereNode LastNeighbour
        {
            get
            {
                if (neighbours.Count == 0)
                    return null;
                else
                    return neighbours.Last<SphereNode>();
            }
        }
        public int NeighbourCount
        {
            get { return neighbours.Count; }
        }
        public bool HasNeighbours
        { get { return neighbours.Count > 0; } }
        private List<SphereNode> children;
        public List<SphereNode> Children
        {
            get
            { return children; }
        }
        public List<SphereNode> Neighbours
        {
            get
            { return neighbours; }
        }
        private List<SphereNode> neighbours;
    }
}
