﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SphereShaker.Core
{
    public class Sphere
    {
        private static double insensitivity = 5E-9;

        #region constructors
        public Sphere(SphereFraction fraction)
        {
            Fraction = fraction;
            this.Position = new Vector(-1D, -1D, -1D);
        }
        public Sphere(SphereFraction fraction, double x, double y)
        {
            Fraction = fraction;
            X = x;
            Y = y;
        }

        public Sphere(SphereFraction fraction, Vector position)
        {
            Fraction = fraction;
            this.Position = position;
        }
        #endregion
        
        #region properties
        public SphereFraction Fraction
        {
            get { return fraction; }
            set 
            { 
                fraction = value;
                radiusBuffer = fraction.Radius;
            }
        }

        private SphereFraction fraction;
        private double radiusBuffer;

        public double Radius
        {
            get
            { return radiusBuffer; }
        }


        public Vector Position;

        public double X
        {
            get { return Position.X; }
            set { Position.X = value; }
        }
        public double Y
        {
            get { return Position.Y; }
            set { Position.Y = value; }
        }
        public double Z
        {
            get { return Position.Z; }
            set { Position.Z = value; }
        }


        public bool BorderLeft
        {
            get { return X == Radius; }
        }
        public bool BorderDown
        {
            get { return Y == Radius; }
        }


        #endregion
        #region relation methods
        /// <summary>
        /// Determines whether given Sphere touches this Sphere
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Touch(Sphere other)
        {
            return Touch(this, other);
        }
        /// <summary>
        /// Determines whether two spheres touch
        /// </summary>
        /// <param name="s1"></param>
        /// <param name="s2"></param>
        /// <returns>True if spheres are touching</returns>
        public static bool Touch(Sphere s1, Sphere s2)
        {
            double distance = ((s1.X - s2.X) * (s1.X - s2.X) + (s1.Y - s2.Y) * (s1.Y - s2.Y)); //todo: vektorově
            if (Math.Abs(distance - (s1.Radius + s2.Radius) * (s1.Radius + s2.Radius)) < insensitivity)
                return true;
            else
                return false;
        }


        public static List<Sphere> Collisions(Sphere s, ICollection<Sphere> check)
        {
            List<Sphere> collisions = new List<Sphere>();
            foreach (var item in check)
            {
                if (s.Collision(item))
                {
                    collisions.Add(item);
                }
            }
            return collisions;
        }
        
        /// <summary>
        /// Collision of two spheres, includes touching
        /// </summary>
        /// <param name="s1"></param>
        /// <param name="s2"></param>
        /// <returns></returns>
        public static bool Collision(Sphere s1, Sphere s2)
        {
            if (Sphere.ReferenceEquals(s1, s2))
                return false;

            double distance = ((s1.X - s2.X) * (s1.X - s2.X) + (s1.Y - s2.Y) * (s1.Y - s2.Y));
            if ((distance - (s1.Radius + s2.Radius)*(s1.Radius + s2.Radius)) < insensitivity)
                return true;
            else
                return false;
        }

        public bool Collision(Sphere s1)
        {
            return Collision(s1, this);
        }

        /// <summary>
        /// Computes the intersection of two touching Spheres
        /// </summary>
        /// <param name="s1"></param>
        /// <param name="s2"></param>
        /// <returns></returns>
        public static Vector TouchPoint(Sphere s1,Sphere s2)
        {
            //TODO: Exception when Spheres do not touch
            return s1.Position+((s2.Position - s1.Position)*(s1.Radius/(s1.Radius+s2.Radius)));
        }

        public static void MoveToTouch (Sphere toMove,Sphere toTouch)
        {
            //obr. na str 19
            Vector diff = toMove.Position-toTouch.Position;
            double dist = diff.Abs();
            double distDiff = toTouch.Radius + toMove.Radius - dist;
            toMove.Position += diff*(distDiff/dist);
        }

        public static void MoveToSide(Sphere toMove, Sphere toTouch)
        {
            //obr. na str 18-19
            //Sphere toMoveBackup = new Sphere(toMove.Fraction, toMove.X, toMove.Y);


            double sqrt = Math.Sqrt((toMove.Radius + toTouch.Radius) * (toMove.Radius + toTouch.Radius) - (toMove.Y - toTouch.Y) * (toMove.Y - toTouch.Y));
                //decide whether to move to left of to right
            if (double.IsNaN(sqrt))
            {
                throw new Exception("unexpected error");
            }
            if (toTouch.X<toMove.X)
            {
                toMove.X = toTouch.X + sqrt;
                
            }
            else
            {
                toMove.X = toTouch.X - sqrt;
            }

            //TODO: deal with equal X-coordinates (should stay or fall?)
            //TODO: side collisions?
        }

        /// <summary>
        /// Posune kouli, aby se dotýkala stěny a nebyla v kolizi s jinou koulí, střed nebude posunut přes hranice zdi
        /// </summary>
        /// <param name="s"></param>
        /// <param name="wallPosition"></param>
        /// <param name="check"></param>
        public static void MoveToWall(Sphere s, double wallPosition, ICollection<Sphere> check)
        {
            //zarovnat ke stěně
            s.X=(s.X>wallPosition) ? wallPosition+s.Radius : wallPosition-s.Radius;
            //zjistit možné kolize
            List<Sphere> collisions = Collisions(s, check);

            if (collisions.Count==0) //je volná ve vzduchu, jenom posuneme na dotek stěny
            {
                return;
            }
            else
            {
                double[] Ycoords = new double[collisions.Count];
                double maxYcoord=0;
                int maxYcoordI = 0;
                for (int i = 0; i < collisions.Count; i++)
                {
                    //str. 24
                    Ycoords[i] = collisions[i].Y + Math.Sqrt(((s.Radius + collisions[i].Radius) * (s.Radius + collisions[i].Radius)) - ((s.X - collisions[i].X) * (s.X - collisions[i].X)));
                    if (Ycoords[i]>maxYcoord)
                    {
                        maxYcoord = Ycoords[i];
                        maxYcoordI = i;
                    }
                }
                s.Y = maxYcoord;
            }
        }

        #endregion
        #region storing


        /// <summary>
        /// Conversts the Sphere to its textual description
        /// </summary>
        /// <returns>String representation of Sphere</returns>
        public string ToString(bool shortForm)
        {
            if (shortForm)
            {
                return string.Format("{0}\t{1}\t{2}", X, Y, Radius);
            }
            else
            {
                return ToString();
            }
        }
        
        
        /// <summary>
        /// Conversts the Sphere to its textual description
        /// </summary>
        /// <returns>String representation of Sphere</returns>
        public override string ToString()
        {
            return string.Format("X: {0}, Y: {1}, R: {2}", X, Y, Radius);
        }
        #endregion
    }
}
