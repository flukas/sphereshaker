﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SphereShaker.Utility;
//using SvgNet;
using System.Xml.Linq;
using System.IO;

namespace SphereShaker.Core
{
    public class NodeContainer : SphereContainer
    {

        /// <summary>
        /// Empties the container (removes all spheres)
        /// </summary>
        public override void Clear()
        {
            base.Clear();
            this.toPopulate.Clear();
        }

        /// <summary>
        /// Holds reference to Logger. Logging does not work when null.
        /// </summary>
        public Logger Log { get; set; }

        /// <summary>
        /// For Fill4
        /// </summary>
        private Stack<SphereNode> toPopulateStack;

        /// <summary>
        /// For Fill3
        /// </summary>
        private Queue<SphereNode> toPopulate;


        // temporarily out of order
        public NodeContainer(int width, int height)
            : base(width, height)
        {

            //throw new NotImplementedException("temporarily out of order");
            this.width = width;
            this.height = height;
            //statistics = new Dictionary<SphereFraction, ulong>();
            //spheres = (List<Sphere>)(new List<Sphere>()); //uncomment!!!!!
            toPopulate = new Queue<SphereNode>();
            toPopulateStack = new Stack<SphereNode>();
        }



        //public List<SphereNode> spheres;
        //public int width;
        //public int height;

        //public void Fill(SphereGenerator g)
        //{
        //    SphereFraction s = g.GetRandom();
        //    //place first sphere to lower left corner
        //    SphereNode first = new SphereNode(s, s.Radius, s.Radius);
        //    spheres.Add(first);

        //    Place(g.GetRandom(), first);

        //    //spheres.Add(Place(g.GetRandom(), spheres[0], spheres[1]));
        //    if (Changed != null)
        //    {
        //        Changed(this, null);
        //    }
        //}

        public void Fill2(SphereGenerator sg)
        {
            //first to the middle
            SphereFraction s = sg.GetRandom();
            SphereNode current = new SphereNode(s, width / 2, height / 2);

            spheres.Add(current);
            //todo: consider stable list for speed
            List<Sphere> Added = new List<Sphere>(1);
            Added.Add(current);
            OnChanged(new ChangedEventArgs(Added, null, null));
            SphereAdd(sg, current);
        }
        //private void AddSphere(Sphere parent, Sphere item)

        private bool WithinBoundaries(SphereNode item)
        {
            return (item.X - item.Radius > 0 && item.Y - item.Radius > 0 && item.X + item.Radius < width && item.Y + item.Radius < height);
        }


        /// <summary>
        /// Checks for collisions and if there are none, adds the sphere
        /// </summary>
        /// <param name="item"></param>
        private void AddSphere(SphereNode item, SphereNode parent)
        {
            //todo: check duplicities
            if (!Collides(item))
            {
                if (WithinBoundaries(item))
                {
                    if (parent != null)
                    {
                        parent.AddChild(item);
                    }
                    spheres.Add(item);
                    if (Log != null)
                    {
                        Log(string.Format("Added {0}", item), LogLevel.debug);
                    }
                    #region add to stats
                    if (!statistics.ContainsKey(item.Fraction))
                    {
                        statistics.Add(item.Fraction, 0);
                        statistics[item.Fraction]++;
                    }
                    else
                    {
                        statistics[item.Fraction]++;
                    }
                    #endregion
                    //TODO: not very nice...
                    toPopulate.Enqueue(item);
                    toPopulateStack.Push(item);

                    //todo: consider stable list for speed
                    List<Sphere> Added = new List<Sphere>(1);
                    Added.Add(item);
                    OnChanged( new ChangedEventArgs(Added, null, null));
                }
            }
            else
            {
                //kolize, závorka jen kvůli brakepointu
            }

        }

        public void Fill3(SphereGenerator sg)
        {
            if (Log != null)
            {
                Log("Start of filling", LogLevel.info);
            }
            //first to the middle
            SphereFraction s = sg.GetRandom();
            SphereNode current = new SphereNode(s, width / 2, height / 2);
            //AddSphere(current);
            AddSphere(current, null);
            if (Log != null)
            {
                Log(string.Format("Added first {0}", current), LogLevel.info);
            }
            while (toPopulate.Count != 0)
            {
                SphereAdd2(sg, toPopulate.Dequeue());

            }
            if (Log != null)
            {
                Log("Filling finished", LogLevel.info);
            }
            OnFilled();

        }

        /// <summary>
        /// Copy of Fill3 with Stack instead of queue. Fills from center by adding new neighbours
        /// </summary>
        /// <param name="sg"></param>
        public void Fill4(SphereGenerator sg)
        {
            if (Log != null)
            {
                Log("Start of filling", LogLevel.info);
            }
            //first to the middle
            SphereFraction s = sg.GetRandom();
            SphereNode current = new SphereNode(s, width / 2, height / 2);
            //AddSphere(current);
            AddSphere(current, null);
            if (Log != null)
            {
                Log(string.Format("Added first {0}", current), LogLevel.info);
            }
            while (toPopulateStack.Count != 0)
            {
                SphereAdd2(sg, toPopulateStack.Pop());

            }
            if (Log != null)
            {
                Log("Filling finished", LogLevel.info);
            }
            OnFilled();

        }

        private bool Exists(SphereNode toCheck)
        {
            foreach (SphereNode s in spheres)
            {
                if (
                    Math.Abs(s.X - toCheck.X) < 1e-10 &&
                    Math.Abs(s.Y - toCheck.Y) < 1e-10 &&
                    s.Radius == toCheck.Radius
                    )
                {
                    return true;
                }
            }
            return false;
        }

        private bool Collides(SphereNode toCheck)
        {
            foreach (SphereNode s in spheres)
            {
                if (Collision(s, toCheck))
                    return true;

            }
            return false;
        }


        public void SphereAdd2(SphereGenerator sg, SphereNode current)
        {
            SphereFraction sf = sg.GetRandom();
            if (current.ChildrenCount != 0)
            {

                if (Log != null)
                {
                    Log(string.Format("{0} has children", current), LogLevel.debug);
                }
                #region has children
                //get possible spheres
                SphereNode[] ss = Place(sf, current, current.LastChild);
                if (Log != null)
                {
                    Log(string.Format("Possible new neighbours: {0} and {1}", ss[0], ss[1]), LogLevel.debug);
                }
                //check for collisions
                bool[] collisions = new bool[ss.Length];
                foreach (SphereNode s in current.Neighbours)
                {
                    for (int i = 0; i < collisions.Length; i++)
                    {
                        //if (Collision(s, ss[i]) || !WithinBoundaries(ss[i]))
                        if (Collides(ss[i]) || !WithinBoundaries(ss[i]))
                        {
                            //nelze dát dohromady, pak se kolidující přepisují na nekolidující
                            collisions[i] = true;
                        }
                    }
                }
                if (Log != null)
                {
                    Log(string.Format("Collisions {0},{1}", collisions[0], collisions[1]), LogLevel.debug);
                }
                //pick sphere to place
                //todo: just randomly?
                //bool found = false; //možná vracet jestli našel?
                for (int i = 0; i < ss.Length; i++)
                {
                    //TODO: container boundaries check
                    if (!collisions[i])
                    {
                        //found = true;

                        //current.AddChild(ss[i]);
                        AddSphere(ss[i], current);

                        //pokusí se doplnit další sousedy k současné kružnici
                        SphereAdd2(sg, current);
                        break;
                    }
                }
                //nic se nenašlo, nic se nepřidalo, nikomu to nevadí
                #endregion
            }
            else if (current.HasNeighbours)
            {
                #region has neighbours
                if (Log != null)
                {
                    Log(string.Format("{0} has neighbours", current), LogLevel.debug);
                }
                //get possible spheres
                SphereNode[] ss = Place(sf, current, current.LastNeighbour);
                if (Log != null)
                {
                    Log(string.Format("Possible new neighbours: {0} and {1}", ss[0], ss[1]), LogLevel.debug);
                }
                //check for collisions
                bool[] collisions = new bool[ss.Length];
                foreach (SphereNode s in current.Neighbours)
                {
                    for (int i = 0; i < collisions.Length; i++)
                    {
                        //if (Collision(s, ss[i]) || !WithinBoundaries(ss[i]))
                        if (Collides(ss[i]) || !WithinBoundaries(ss[i]))
                        {
                            //nelze dát dohromady, pak se kolidující přepisují na nekolidující
                            collisions[i] = true;
                        }
                    }
                }
                if (Log != null)
                {
                    Log(string.Format("Collisions {0},{1}", collisions[0], collisions[1]), LogLevel.debug);
                }
                //pick sphere to place
                //todo: just randomly?
                //bool found = false; //možná vracet jestli našel?
                for (int i = 0; i < ss.Length; i++)
                {
                    //TODO: container boundaries check
                    if (!collisions[i])
                    {
                        //found = true;

                        //current.AddChild(ss[i]);
                        AddSphere(ss[i], current);
                        //pokusí se doplnit další sousedy k současné kružnici
                        SphereAdd2(sg, current);
                        break;
                    }
                }
                //nic se nenašlo, nic se nepřidalo, nikomu to nevadí
                #endregion
            }
            else
            {
                #region has no friends :(
                if (Log != null)
                {
                    Log(string.Format("{0} has no friends", current), LogLevel.debug);
                }
                //add sphere next to this one
                //todo: random orientation
                SphereNode s = new SphereNode(sf, current.X, current.Y + current.Radius + sf.Radius);
                //current.AddChild(s);
                AddSphere(s, current);

                SphereAdd2(sg, current);
                #endregion
            }
        }

        public void SphereAdd3(SphereGenerator sg, SphereNode current)
        {
            SphereFraction sf = sg.GetRandom();
            if (current.ChildrenCount != 0)
            {

                if (Log != null)
                {
                    Log(string.Format("{0} has children", current), LogLevel.debug);
                }
                #region has children
                //get possible spheres
                SphereNode[] ss = Place(sf, current, current.LastChild);
                if (Log != null)
                {
                    Log(string.Format("Possible new neighbours: {0} and {1}", ss[0], ss[1]), LogLevel.debug);
                }
                //check for collisions
                bool[] collisions = new bool[ss.Length];
                foreach (SphereNode s in current.Neighbours)
                {
                    for (int i = 0; i < collisions.Length; i++)
                    {
                        //if (Collision(s, ss[i]) || !WithinBoundaries(ss[i]))
                        if (Collides(ss[i]) || !WithinBoundaries(ss[i]))
                        {
                            //nelze dát dohromady, pak se kolidující přepisují na nekolidující
                            collisions[i] = true;
                        }
                    }
                }
                if (Log != null)
                {
                    Log(string.Format("Collisions {0},{1}", collisions[0], collisions[1]), LogLevel.debug);
                }
                //pick sphere to place
                //todo: just randomly?
                //bool found = false; //možná vracet jestli našel?
                for (int i = 0; i < ss.Length; i++)
                {
                    //TODO: container boundaries check
                    if (!collisions[i])
                    {
                        //found = true;

                        //current.AddChild(ss[i]);
                        AddSphere(ss[i], current);

                        //pokusí se doplnit další sousedy k současné kružnici
                        SphereAdd2(sg, current);
                        break;
                    }
                }
                //nic se nenašlo, nic se nepřidalo, nikomu to nevadí
                #endregion
            }
            else if (current.HasNeighbours)
            {
                #region has neighbours
                if (Log != null)
                {
                    Log(string.Format("{0} has neighbours", current), LogLevel.debug);
                }
                //get possible spheres
                SphereNode[] ss = Place(sf, current, current.LastNeighbour);
                if (Log != null)
                {
                    Log(string.Format("Possible new neighbours: {0} and {1}", ss[0], ss[1]), LogLevel.debug);
                }
                //check for collisions
                bool[] collisions = new bool[ss.Length];
                foreach (SphereNode s in current.Neighbours)
                {
                    for (int i = 0; i < collisions.Length; i++)
                    {
                        //if (Collision(s, ss[i]) || !WithinBoundaries(ss[i]))
                        if (Collides(ss[i]) || !WithinBoundaries(ss[i]))
                        {
                            //nelze dát dohromady, pak se kolidující přepisují na nekolidující
                            collisions[i] = true;
                        }
                    }
                }
                if (Log != null)
                {
                    Log(string.Format("Collisions {0},{1}", collisions[0], collisions[1]), LogLevel.debug);
                }
                //pick sphere to place
                //todo: just randomly?
                //bool found = false; //možná vracet jestli našel?
                for (int i = 0; i < ss.Length; i++)
                {
                    //TODO: container boundaries check
                    if (!collisions[i])
                    {
                        //found = true;

                        //current.AddChild(ss[i]);
                        AddSphere(ss[i], current);
                        //pokusí se doplnit další sousedy k současné kružnici
                        SphereAdd2(sg, current);
                        break;
                    }
                }
                //nic se nenašlo, nic se nepřidalo, nikomu to nevadí
                #endregion
            }
            else
            {
                #region has no friends :(
                if (Log != null)
                {
                    Log(string.Format("{0} has no friends", current), LogLevel.debug);
                }
                //add sphere next to this one
                //todo: random orientation
                SphereNode s = new SphereNode(sf, current.X, current.Y + current.Radius + sf.Radius);
                //current.AddChild(s);
                AddSphere(s, current);

                SphereAdd2(sg, current);
                #endregion
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sg"></param>
        /// <param name="current"></param>
        public void SphereAdd(SphereGenerator sg, SphereNode current)
        //todo: remove code duplicities
        {
            SphereFraction sf = sg.GetRandom();
            if (current.ChildrenCount == 0)
            {
                //add sphere next to this one
                //todo: random orientation
                SphereNode s = new SphereNode(sf, current.X, current.Y + current.Radius + sf.Radius);
                current.AddChild(s);
                spheres.Add(s);

                //todo: consider stable list for speed
                List<Sphere> Added = new List<Sphere>(1);
                Added.Add(s);
                OnChanged( new ChangedEventArgs(Added, null, null));
                
                SphereAdd(sg, current);
            }
            else
            {
                //get possible spheres
                SphereNode[] ss = Place(sf, current, current.LastChild);
                //check for collisions
                bool[] collisions = new bool[ss.Length];
                SphereNode[] colliders = new SphereNode[ss.Length];
                foreach (SphereNode s in current.Neighbours)
                {
                    for (int i = 0; i < collisions.Length; i++)
                    {
                        if (Collision(s, ss[i]))
                        {
                            collisions[i] = true;
                            //zkusíme co to udělá s přepisováním
                            colliders[i] = s;
                            ////nepřepisovat první nalezenou kolizi, aby se předala nejnižší a ne kolize s předposledním ptomkem
                        }
                    }
                }
                //pick sphere to place
                bool found = false;
                for (int i = 0; i < ss.Length; i++)
                {
                    //TODO: container boundaries check
                    if (!collisions[i])
                    {
                        current.AddChild(ss[i]);
                        spheres.Add(ss[i]);


                        //todo: consider stable list for speed
                        List<Sphere> Added = new List<Sphere>(1);
                        Added.Add(ss[i]);
                        OnChanged( new ChangedEventArgs(Added, null, null));


                        SphereAdd(sg, current);
                        found = true;

                        break;
                    }
                }

                //todo:přidat zmenšování
                if (!found)
                {
                    SphereNode old = current;
                    current = colliders[0];

                    //get possible spheres
                    SphereNode[] ss2 = Place(sf, current, old.LastChild);
                    //check for collisions
                    bool[] collisions2 = new bool[ss2.Length];
                    SphereNode[] colliders2 = new SphereNode[ss2.Length];
                    foreach (SphereNode s in current.Neighbours)
                    {
                        for (int i = 0; i < collisions2.Length; i++)
                        {
                            if (Collision(s, ss2[i]))
                            {
                                collisions2[i] = true;
                                //zkusíme co to udělá s přepisováním
                                colliders2[i] = s;
                                ////nepřepisovat první nalezenou kolizi, aby se předala nejnižší a ne kolize s předposledním ptomkem
                            }
                        }
                    }
                    //pick sphere to place
                    bool found2 = false;
                    for (int i = 0; i < ss2.Length; i++)
                    {
                        //TODO: container boundaries check
                        if (!collisions2[i])
                        {
                            current.AddChild(ss2[i]);
                            spheres.Add(ss2[i]);


                            //todo: consider stable list for speed
                            List<Sphere> Added = new List<Sphere>(1);
                            Added.Add(ss2[i]);
                            OnChanged( new ChangedEventArgs(Added, null, null));

                            SphereAdd(sg, current);
                            found2 = true;
                            break;
                        }
                    }
                    if (!found2)
                    {
                        throw new Exception("shit!");
                    }
                }
            }
        }

        private void Place(SphereFraction sf, SphereNode parent)
        {
            //first in tree
            if (parent.Neighbours.Count == 0)
            {
                if (parent.BorderLeft)
                {
                    //TODO: faster?
                    //poloměr kružnice na níž leží střed nové kružnice
                    //odvození na str 2.
                    double R12 = parent.Radius + sf.Radius;
                    double R2_X1 = sf.Radius - parent.X;
                    SphereNode s = new SphereNode(sf,
                        sf.Radius,
                        Math.Sqrt(R12 * R12 - R2_X1 * R2_X1) + parent.Y
                        );
                    parent.Neighbours.Add(s);
                    spheres.Add(s);
                }
                //TODO: Add if borders on other sides
            }
            //not first in tree
            else
            {

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="toPlace"></param>
        /// <param name="s1">parent</param>
        /// <param name="s2">neighbour</param>
        /// <returns></returns>
        private SphereNode[] Place(SphereFraction toPlace, SphereNode s1, SphereNode s2)
        //todo: use inherited version from parent class
        {
            
            //Odvození strany 7 a násl.

            double r13s = (s1.Radius + toPlace.Radius) * (s1.Radius + toPlace.Radius);
            double r23s = (s2.Radius + toPlace.Radius) * (s2.Radius + toPlace.Radius);

            double xc = 2 * (s2.X - s1.X);
            double yc = 2 * (s2.Y - s1.Y);
            if (yc == 0)
            {
                throw new Exception("prohodit souřadnice či co");
            }
            double cc = (s1.X * s1.X) - (s2.X * s2.X) + (s1.Y * s1.Y) - (s2.Y * s2.Y) - r13s + r23s;

            double xDy = xc / yc;
            double cDy = cc / yc;

            //parametry kvadratické rovnice
            double a = 1 + (xDy * xDy);
            double b = 2 * (xDy * cDy + s1.Y * xDy - s1.X);
            double c = s1.X * s1.X + cDy * cDy + 2 * s1.Y * cDy + s1.Y * s1.Y - r13s;

            double[] Xs = SolveQuadratic(a, b, c);
            double[] Ys = new double[Xs.Length];
            for (int i = 0; i < Xs.Length; i++)
            {
                Ys[i] = -(cDy + xDy * Xs[i]);
            }
            SphereNode[] spheres = new SphereNode[2];
            spheres[0] = new SphereNode(toPlace, Xs[0], Ys[0]);
            spheres[1] = new SphereNode(toPlace, Xs[1], Ys[1]);
            return spheres;

            //#region collision check
            //bool [] collisions = {false,false};

            //foreach (Sphere  s in s1.Neighbours)
            //{
            //    collisions [0] = Collision(s,spheres [0]);
            //    collisions [1] = Collision(s,spheres [1]);

            //}
            //#endregion

            //#region boundaries
            //if (Xs.Length == 1)
            //{
            //    return new Sphere(toPlace, Xs[0], Ys[0]);
            //}

            //else if (Ys[0] < 0 || Xs[0] < 0)
            //{
            //    return new Sphere(toPlace, Xs[1], Ys[1]);
            //}
            //else if (Ys[1] < 0 || Xs[1] < 0)
            //{
            //    return new Sphere(toPlace, Xs[0], Ys[0]);
            //}
            //else if (Ys[0] < Ys[1])
            ////Choose lowest Y coordinate
            //{
            //    return new Sphere(toPlace, Xs[0], Ys[0]);
            //}
            //else 
            //{
            //    return new Sphere(toPlace,Xs[1], Ys[1]);
            //}
            //#endregion
        }

        private bool Collision(SphereNode s1, SphereNode s2)
        {
            double distance = Math.Sqrt((s1.X - s2.X) * (s1.X - s2.X) + (s1.Y - s2.Y) * (s1.Y - s2.Y));
            if ((distance - (s1.Radius + s2.Radius)) < -1E-8)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Solves qudratic equation in format a*x^2+b*x+c=0
        /// </summary>
        /// <param name="a">parameter for x^2</param>
        /// <param name="b">parameter for x</param>
        /// <param name="c">absolute member</param>
        /// <returns>Array of one or two solution</returns>
        private double[] SolveQuadratic(double a, double b, double c)
        {
            return Utility.AnalyticGeometry.SolveQuadratic(a, b, c);

        }

        public override void Fill(SphereGenerator g)
        {
            Fill3(g);
        }
    }
}