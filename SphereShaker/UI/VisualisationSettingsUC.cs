﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SphereShaker.UI
{
    public partial class VisualisationSettingsUC : UserControl
    {
        public VisualisationSettingsUC()
        {
            InitializeComponent();
        }
        private SphereVisualiser sv;
        public SphereVisualiser Visualiser 
        {
            get { return sv; }
            set {
                sv = value;
                Load();
            }
        }

        private void Load()
        {
            if (sv != null)
            {
                this.cbCenters.Checked = sv.Centers;
                this.cbCircles.Checked = sv.Circles;
                //cbChildLines.Checked = sv.ChildLines;
                //cbChildren.Checked = sv.Children;
                //cbNeighbourLines.Checked = sv.NeighbourLines;
                //cbNumbers.Checked = sv.Numbers;
                //cbParentLines.Checked = sv.ParentLines;
                cbStatistics.Checked = sv.Statistics;
            }
        }

        private void VisualisationSettingsUC_Paint(object sender, PaintEventArgs e)
        {
            this.Load();
        }

        private void cbNeighbourLines_CheckedChanged(object sender, EventArgs e)
        {
            //sv.NeighbourLines = cbNeighbourLines.Checked;
            //if (Changed != null)
            //{
            //    Changed(this, null);
            //}
        }

        private void cbParentLines_CheckedChanged(object sender, EventArgs e)
        {
            //sv.ParentLines = cbParentLines.Checked;
            //if (Changed != null)
            //{
            //    Changed(this, null);
            //}
        }

        private void cbChildLines_CheckedChanged(object sender, EventArgs e)
        {
            //sv.ChildLines = cbChildLines.Checked;
            //if (Changed != null)
            //{
            //    Changed(this, null);
            //}
        }

        private void cbNumbers_CheckedChanged(object sender, EventArgs e)
        {
            //sv.Numbers = cbNumbers.Checked;
            //if (Changed != null)
            //{
            //    Changed(this, null);
            //}
        }

        private void cbStatistics_CheckedChanged(object sender, EventArgs e)
        {
            sv.Statistics = cbStatistics.Checked;
            if (Changed != null)
            {
                Changed(this, null);
            }
        }

        private void cbCenters_CheckedChanged(object sender, EventArgs e)
        {
            sv.Centers = cbCenters.Checked;
            if (Changed != null)
            {
                Changed(this, null);
            }
        }

        private void cbCircles_CheckedChanged(object sender, EventArgs e)
        {
            sv.Circles = cbCircles.Checked;
            if (Changed != null)
            {
                Changed(this, null);
            }
        }

        private void cbChildren_CheckedChanged(object sender, EventArgs e)
        {
            //sv.Children = cbChildren.Checked;
            //if (Changed != null)
            //{
            //    Changed(this, null);
            //}
        }
        public event EventHandler Changed;
    }
}
