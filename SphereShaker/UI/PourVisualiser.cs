﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SphereShaker.Core;

namespace SphereShaker.UI
{
    public partial class PourVisualiser : SphereVisualiser
    {
        public PourVisualiser()
        {
            InitializeComponent();

        }


        #region colours
        static readonly Color cLastLayer = Color.Green;
        static readonly Pen pLastLayer = new Pen(cLastLayer, 2);
        #endregion
        protected override void Draw(PaintEventArgs e)
        {
            base.Draw(e);
            Graphics g = e.Graphics;
            //identification


            g.DrawString("Pour Visualiser", fText, bText, new Point(10, 70));

            //draw last layer
            //return;
            if (c==null)
            {
                return;
            }
            //tady se zatoulá, když je c==null
            SortedList<double,Sphere> ll = ((PourContainer)c).lastLayer;

            PointF [] points = new PointF[ll.Count];

            for (int i = 0; i < ll.Count; i++)
            {
                points[i] = GetCenterPoint(ll.ElementAt(i).Value);
            }
            if (points.Length>=2)
            {
                g.DrawLines(pLastLayer, points);
            }
            
            

        }

        void container_Changed(object sender, ChangedEventArgs ce)
        {
            
        }

    }
}
