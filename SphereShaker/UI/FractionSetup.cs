﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SphereShaker.Core;

namespace SphereShaker.UI
{
    public partial class FractionSetup : Form
    {
        public FractionSetup(SphereGenerator sg)
        {
            InitializeComponent();
            //Generator = new SphereGenerator(sg);

            mySg = new SphereShaker.UI.SphereGeneratorUC(sg);
            mySg.Dock = System.Windows.Forms.DockStyle.Fill;
            mySg.Location = new System.Drawing.Point(0, 0);
            mySg.Name = "mySg";
            mySg.Size = new System.Drawing.Size(288, 295);
            mySg.TabIndex = 0;
            //sphereController1 = new SphereGeneratorUC(sg);
            this.splitContainer1.Panel1.Controls.Add(this.mySg);
        }
        private SphereGeneratorUC mySg;
        
        public SphereGenerator Generator
        {
            get
            {
                return this.mySg.Generator;
            }
            set
            { 
                this.mySg.Generator = value;
            }
        }
        
        //public List<SphereFraction> Fractions
        //{
        //    get
        //    {
        //        return this.sphereController1.Fractions;
        //    }
        //    set
        //    { this.sphereController1.Fractions = value; }
        //}

        private void okButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;

            this.Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
