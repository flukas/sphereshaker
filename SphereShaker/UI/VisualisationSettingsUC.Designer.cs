﻿namespace SphereShaker.UI
{
    partial class VisualisationSettingsUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbNeighbourLines = new System.Windows.Forms.CheckBox();
            this.cbParentLines = new System.Windows.Forms.CheckBox();
            this.cbChildLines = new System.Windows.Forms.CheckBox();
            this.cbNumbers = new System.Windows.Forms.CheckBox();
            this.cbStatistics = new System.Windows.Forms.CheckBox();
            this.cbCenters = new System.Windows.Forms.CheckBox();
            this.cbCircles = new System.Windows.Forms.CheckBox();
            this.cbChildren = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // cbNeighbourLines
            // 
            this.cbNeighbourLines.AutoSize = true;
            this.cbNeighbourLines.Enabled = false;
            this.cbNeighbourLines.Location = new System.Drawing.Point(4, 4);
            this.cbNeighbourLines.Name = "cbNeighbourLines";
            this.cbNeighbourLines.Size = new System.Drawing.Size(64, 17);
            this.cbNeighbourLines.TabIndex = 0;
            this.cbNeighbourLines.Text = "Sousedi";
            this.cbNeighbourLines.UseVisualStyleBackColor = true;
            this.cbNeighbourLines.CheckedChanged += new System.EventHandler(this.cbNeighbourLines_CheckedChanged);
            // 
            // cbParentLines
            // 
            this.cbParentLines.AutoSize = true;
            this.cbParentLines.Enabled = false;
            this.cbParentLines.Location = new System.Drawing.Point(4, 28);
            this.cbParentLines.Name = "cbParentLines";
            this.cbParentLines.Size = new System.Drawing.Size(84, 17);
            this.cbParentLines.TabIndex = 1;
            this.cbParentLines.Text = "Spojit rodiče";
            this.cbParentLines.UseVisualStyleBackColor = true;
            this.cbParentLines.CheckedChanged += new System.EventHandler(this.cbParentLines_CheckedChanged);
            // 
            // cbChildLines
            // 
            this.cbChildLines.AutoSize = true;
            this.cbChildLines.Enabled = false;
            this.cbChildLines.Location = new System.Drawing.Point(4, 52);
            this.cbChildLines.Name = "cbChildLines";
            this.cbChildLines.Size = new System.Drawing.Size(95, 17);
            this.cbChildLines.TabIndex = 2;
            this.cbChildLines.Text = "Spojit potomky";
            this.cbChildLines.UseVisualStyleBackColor = true;
            this.cbChildLines.CheckedChanged += new System.EventHandler(this.cbChildLines_CheckedChanged);
            // 
            // cbNumbers
            // 
            this.cbNumbers.AutoSize = true;
            this.cbNumbers.Enabled = false;
            this.cbNumbers.Location = new System.Drawing.Point(4, 76);
            this.cbNumbers.Name = "cbNumbers";
            this.cbNumbers.Size = new System.Drawing.Size(50, 17);
            this.cbNumbers.TabIndex = 3;
            this.cbNumbers.Text = "Čísla";
            this.cbNumbers.UseVisualStyleBackColor = true;
            this.cbNumbers.CheckedChanged += new System.EventHandler(this.cbNumbers_CheckedChanged);
            // 
            // cbStatistics
            // 
            this.cbStatistics.AutoSize = true;
            this.cbStatistics.Location = new System.Drawing.Point(4, 100);
            this.cbStatistics.Name = "cbStatistics";
            this.cbStatistics.Size = new System.Drawing.Size(68, 17);
            this.cbStatistics.TabIndex = 4;
            this.cbStatistics.Text = "Statistiky";
            this.cbStatistics.UseVisualStyleBackColor = true;
            this.cbStatistics.CheckedChanged += new System.EventHandler(this.cbStatistics_CheckedChanged);
            // 
            // cbCenters
            // 
            this.cbCenters.AutoSize = true;
            this.cbCenters.Location = new System.Drawing.Point(4, 124);
            this.cbCenters.Name = "cbCenters";
            this.cbCenters.Size = new System.Drawing.Size(57, 17);
            this.cbCenters.TabIndex = 5;
            this.cbCenters.Text = "Středy";
            this.cbCenters.UseVisualStyleBackColor = true;
            this.cbCenters.CheckedChanged += new System.EventHandler(this.cbCenters_CheckedChanged);
            // 
            // cbCircles
            // 
            this.cbCircles.AutoSize = true;
            this.cbCircles.Location = new System.Drawing.Point(4, 148);
            this.cbCircles.Name = "cbCircles";
            this.cbCircles.Size = new System.Drawing.Size(67, 17);
            this.cbCircles.TabIndex = 6;
            this.cbCircles.Text = "Kružnice";
            this.cbCircles.UseVisualStyleBackColor = true;
            this.cbCircles.CheckedChanged += new System.EventHandler(this.cbCircles_CheckedChanged);
            // 
            // cbChildren
            // 
            this.cbChildren.AutoSize = true;
            this.cbChildren.Enabled = false;
            this.cbChildren.Location = new System.Drawing.Point(4, 172);
            this.cbChildren.Name = "cbChildren";
            this.cbChildren.Size = new System.Drawing.Size(64, 17);
            this.cbChildren.TabIndex = 7;
            this.cbChildren.Text = "Potomci";
            this.cbChildren.UseVisualStyleBackColor = true;
            this.cbChildren.CheckedChanged += new System.EventHandler(this.cbChildren_CheckedChanged);
            // 
            // VisualisationSettingsUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.cbChildren);
            this.Controls.Add(this.cbCircles);
            this.Controls.Add(this.cbCenters);
            this.Controls.Add(this.cbStatistics);
            this.Controls.Add(this.cbNumbers);
            this.Controls.Add(this.cbChildLines);
            this.Controls.Add(this.cbParentLines);
            this.Controls.Add(this.cbNeighbourLines);
            this.Name = "VisualisationSettingsUC";
            this.Size = new System.Drawing.Size(150, 204);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.VisualisationSettingsUC_Paint);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox cbNeighbourLines;
        private System.Windows.Forms.CheckBox cbParentLines;
        private System.Windows.Forms.CheckBox cbChildLines;
        private System.Windows.Forms.CheckBox cbNumbers;
        private System.Windows.Forms.CheckBox cbStatistics;
        private System.Windows.Forms.CheckBox cbCenters;
        private System.Windows.Forms.CheckBox cbCircles;
        private System.Windows.Forms.CheckBox cbChildren;
    }
}
