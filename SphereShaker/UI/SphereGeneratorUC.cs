﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SphereShaker.Core;

namespace SphereShaker.UI
{
    public partial class SphereGeneratorUC : UserControl
    {
        public SphereGeneratorUC()
        {
            InitializeComponent();
            //fractionUCs = new List<FractionUC>();
            Generator = new SphereGenerator();
            for (int i = 0; i < InitialCount; i++)
            {
                Generator.Add(new SphereFraction(10,1));
            }
            //CreateFractionUCs();
            //AddFractionUCs(InitialCount);
            PlaceFractionUCs();
        }
        public SphereGeneratorUC(int fractionsCount)
        {
            
            InitializeComponent();
            //fractionUCs = new List<FractionUC>();
            Generator = new SphereGenerator();
            for (int i = 0; i < fractionsCount; i++)
            {
                Generator.Add(new SphereFraction(10, 1));
            }
            
            //AddFractionUCs(fractionsCount);
            PlaceFractionUCs();
        }

        public SphereGeneratorUC(SphereGenerator gen)
        {
            InitializeComponent();
            Generator = new SphereGenerator(gen);//same, but not reference equal
            //CreateFractionUCs();
            PlaceFractionUCs();
        }

        public int InitialCount
        { get; set; }
        ///// <summary>
        ///// Number of Fractions
        ///// </summary>
        //public int Count
        //{ get; set; }

        ///// <summary>
        ///// Fractions of this control
        ///// </summary>
        //public List<SphereFraction> Fractions
        //{
        //    get;
        //    set;
        //}
        /// <summary>
        /// Generator of this control
        /// </summary>
        public SphereGenerator Generator { get; set; }
        
        private List<FractionUC> fractionUCs;

        private void CreateFractionUCs()
        {
            this.splitContainer1.Panel2.Controls.Clear();
            fractionUCs = new List<FractionUC>();
            foreach (var item in Generator.Fractions)
            {
                FractionUC f = new FractionUC(item);
                
                //f.Fraction = item;
                fractionUCs.Add(f);
            }
        }

        private void PlaceFractionUCs()
        {
            CreateFractionUCs();
            //TODO: přidat suspendlayout
            for (int i = 0; i < fractionUCs.Count; i++)
            {
                fractionUCs[i].Size = new Size(250, 26);
                fractionUCs[i].Location = new Point(6, i * 26);
                fractionUCs[i].Name = "Fraction" + i.ToString();
                fractionUCs[i].Title = "Frakce " + i.ToString();
                fractionUCs[i].TabIndex = i;

                this.splitContainer1.Panel2.Controls.Add(fractionUCs[i]);
            }
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            //AddFractionUCs(1)
            //proč tady není aspoň jeden rámeček?
            Generator.Add(new SphereFraction(10,1));
            //CreateFractionUCs();
            PlaceFractionUCs();
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            if (Generator.FractionCount > 0)
            {
                Generator.Remove();

                //this.splitContainer1.Panel2.Controls.RemoveAt(splitContainer1.Panel2.Controls.Count-1);
                this.splitContainer1.Panel2.Controls.Clear();
                //CreateFractionUCs();
                //FractionUC rem = fractionUCs.Last<FractionUC>();
                //this.splitContainer1.Panel2.Controls.Remove(rem);
                //fractionUCs.Remove(rem);

                //Fractions.Remove(rem.Fraction);

                PlaceFractionUCs();
            }

        }

    }
}
