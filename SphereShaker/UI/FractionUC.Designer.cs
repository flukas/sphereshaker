﻿namespace SphereShaker.UI
{
    partial class FractionUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fractionLabel = new System.Windows.Forms.Label();
            this.nudProbability = new System.Windows.Forms.NumericUpDown();
            this.nudDiameter = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.nudProbability)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDiameter)).BeginInit();
            this.SuspendLayout();
            // 
            // fractionLabel
            // 
            this.fractionLabel.AutoSize = true;
            this.fractionLabel.Location = new System.Drawing.Point(3, 5);
            this.fractionLabel.Name = "fractionLabel";
            this.fractionLabel.Size = new System.Drawing.Size(40, 13);
            this.fractionLabel.TabIndex = 0;
            this.fractionLabel.Text = "Frakce";
            // 
            // nudProbability
            // 
            this.nudProbability.AccessibleName = "Probability";
            this.nudProbability.DecimalPlaces = 2;
            this.nudProbability.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudProbability.Location = new System.Drawing.Point(167, 3);
            this.nudProbability.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudProbability.Name = "nudProbability";
            this.nudProbability.Size = new System.Drawing.Size(80, 20);
            this.nudProbability.TabIndex = 2;
            this.nudProbability.ValueChanged += new System.EventHandler(this.nudProbability_ValueChanged);
            this.nudProbability.Validated += new System.EventHandler(this.nudProbability_ValueChanged);
            this.nudProbability.Leave += new System.EventHandler(this.nudProbability_ValueChanged);
            // 
            // nudDiameter
            // 
            this.nudDiameter.AccessibleName = "Diameter";
            this.nudDiameter.DecimalPlaces = 2;
            this.nudDiameter.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudDiameter.Location = new System.Drawing.Point(81, 3);
            this.nudDiameter.Name = "nudDiameter";
            this.nudDiameter.Size = new System.Drawing.Size(80, 20);
            this.nudDiameter.TabIndex = 1;
            this.nudDiameter.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudDiameter.ValueChanged += new System.EventHandler(this.nudDiameter_ValueChanged);
            this.nudDiameter.Validated += new System.EventHandler(this.nudDiameter_ValueChanged);
            this.nudDiameter.Leave += new System.EventHandler(this.nudDiameter_ValueChanged);
            // 
            // FractionUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.nudProbability);
            this.Controls.Add(this.nudDiameter);
            this.Controls.Add(this.fractionLabel);
            this.Name = "FractionUC";
            this.Size = new System.Drawing.Size(250, 26);
            ((System.ComponentModel.ISupportInitialize)(this.nudProbability)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDiameter)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label fractionLabel;
        private System.Windows.Forms.NumericUpDown nudDiameter;
        private System.Windows.Forms.NumericUpDown nudProbability;
    }
}
