﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SphereShaker.Core
{
    class SpherePacker //is proabably a useless class
    {
        public NodeContainer Container;
        public SphereGenerator sg
        {
            get
            {
                return sg;
            }
            set
            {
                sg = value;
            }
        }
        /// <summary>
        /// Fills the container by default method using SphereGenerator sg
        /// </summary>
        public void Fill()
        {
            this.Container.Clear();
            this.Container.Fill3(sg);
        }
    }
}
