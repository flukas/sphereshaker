﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing.Imaging;
using SphereShaker.Utility;

namespace SphereShaker.Core
{
    public class SphereMovedEventArgs:EventArgs 
    {
        public Sphere s;

        public SphereMovedEventArgs (Sphere s)
	{
            this.s =s;
	}
    }
    
    public abstract class SphereContainer
    {
        #region constructors

        public SphereContainer(int width, int height)
        {
            this.width = width;
            this.height = height;
            spheres = new List<Sphere>();
            statistics = new Dictionary<SphereFraction, ulong>();
        }
        #endregion
        #region container properties
        /// <summary>
        /// Gets or sets width (X-dimension) of the Container
        /// </summary>
        public int Width
        {
            get { return width; }
            set { width = value; }
        }
        protected int width;
        
        
        /// <summary>
        /// Gets or sets the height (Y dimension) of the Container
        /// </summary>
        public int Height
        {
            get { return height; }
            set { height = value; }
        }
        protected int height;


        /// <summary>
        /// Gets volume of the container (width*height)
        /// </summary>
        public double TotalSpace
        {
            get { return width * height; }
        }
        #endregion
        #region content
        /// <summary>
        /// Gets the total number of spheres in container
        /// </summary>
        public ulong Spheres
        {
            get
            {
                ulong s = 0;
                foreach (var cnt in statistics.Values)
                {
                    s += cnt;
                }
                return s;
            }
        }
        
        public List<Sphere> spheres;


        /// <summary>
        /// Holds quantity of different fractions
        /// </summary>
        protected Dictionary<SphereFraction, ulong> statistics;


        /// <summary>
        /// Returns the space Filled by the spheres
        /// </summary>
        public double FilledSpace
        {
            get
            {
                double filledSpace = 0;
                foreach (var item in statistics)
                {
                    filledSpace += item.Key.Surface * item.Value;
                }
                return filledSpace;
            }
        }
        /// <summary>
        /// Gets the space not occupied by spheres
        /// </summary>
        public double EmptySpace
        {
            get
            {
                return TotalSpace - FilledSpace;
            }
        }
        /// <summary>
        /// Gets the ratio of filled space
        /// </summary>
        public double FillFactor
        {
            get
            {
                return FilledSpace / TotalSpace;
            }

        }
        #endregion

        #region utility methods
        /// <summary>
        /// Checks for existence of the same sphere in this container
        /// </summary>
        /// <param name="toCheck"></param>
        /// <returns></returns>
        private bool Exists(Sphere toCheck)
        {
            foreach (Sphere s in spheres)
            {
                if (
                    Math.Abs(s.X - toCheck.X) < 1e-10 &&
                    Math.Abs(s.Y - toCheck.Y) < 1e-10 &&
                    s.Fraction == toCheck.Fraction
                    )
                {
                    return true;
                }
            }
            return false;
        }

        public virtual List<Sphere> Collisions(Sphere s)
        {
            return Sphere.Collisions(s, spheres);
        }


        protected virtual bool Collides(Sphere toCheck)
        {
            foreach (Sphere s in spheres)
            {
                if (Sphere.Collision(s, toCheck))
                    return true;

            }
            return false;
        }


        #endregion
        #region spheres manipulation methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="toPlace"></param>
        /// <param name="s1">Sphere 1</param>
        /// <param name="s2">Sphere 2</param>
        /// <returns></returns>
        protected List<Sphere> PlaceToTouchTwo(SphereFraction toPlace, Sphere sphere1, Sphere sphere2)
        {
            //Odvození strany 7 a násl.

            Sphere s1 = new Sphere(sphere1.Fraction, sphere1.Position);
            Sphere s2 = new Sphere(sphere2.Fraction, sphere2.Position);

            bool coordinatesSwitch = false;//incase yc==0 to prevent xc/0 and cc/0

            double r13s = (s1.Radius + toPlace.Radius) * (s1.Radius + toPlace.Radius);
            double r23s = (s2.Radius + toPlace.Radius) * (s2.Radius + toPlace.Radius);

            double xc = 2 * (s2.X - s1.X);
            double yc = 2 * (s2.Y - s1.Y);
            if (Math.Abs(yc)<Math.Abs(xc))
            //Coordinates switching - divisoin by zero and more accuracy
            {
                
                coordinatesSwitch = true;
                double temp = xc;
                xc = yc;
                yc = temp;

                temp = s1.X;
                s1.X = s1.Y;
                s1.Y = temp;

                temp = s2.X;
                s2.X = s2.Y;
                s2.Y = temp;
            }
            double cc = (s1.X * s1.X) - (s2.X * s2.X) + (s1.Y * s1.Y) - (s2.Y * s2.Y) - r13s + r23s;

            double xDy = xc / yc;
            double cDy = cc / yc;

            //parametry kvadratické rovnice
            double a = 1 + (xDy * xDy);
            double b = 2 * (xDy * cDy + s1.Y * xDy - s1.X);
            double c = s1.X * s1.X + cDy * cDy + 2 * s1.Y * cDy + s1.Y * s1.Y - r13s;

            double[] Xs = Utility.AnalyticGeometry.SolveQuadratic(a, b, c);
            double[] Ys = new double[Xs.Length];
            for (int i = 0; i < Xs.Length; i++)
            {
                Ys[i] = -(cDy + xDy * Xs[i]);
            }
            List<Sphere> spheres = new List<Sphere>(2);
            //todo: oštřit jedno možné řešení
            if (!coordinatesSwitch)
            {
                spheres.Add(new Sphere(toPlace, Xs[0], Ys[0]));
                spheres.Add(new Sphere(toPlace, Xs[1], Ys[1]));
            }
            else
            {
                spheres.Add( new Sphere(toPlace, Ys[0], Xs[0]));
                spheres.Add( new Sphere(toPlace, Ys[1], Xs[1]));
            }

            return spheres;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="toPlace"></param>
        /// <param name="s1">parent</param>
        /// <param name="s2">neighbour</param>
        /// <returns></returns>
        protected Sphere[] Place(SphereFraction toPlace, Sphere s1, Sphere s2)
        {
            //Odvození strany 7 a násl.

            double r13s = (s1.Radius + toPlace.Radius) * (s1.Radius + toPlace.Radius);
            double r23s = (s2.Radius + toPlace.Radius) * (s2.Radius + toPlace.Radius);

            double xc = 2 * (s2.X - s1.X);
            double yc = 2 * (s2.Y - s1.Y);
            if (yc == 0)
            {
                throw new Exception("prohodit souřadnice či co");
            }
            double cc = (s1.X * s1.X) - (s2.X * s2.X) + (s1.Y * s1.Y) - (s2.Y * s2.Y) - r13s + r23s;

            double xDy = xc / yc;
            double cDy = cc / yc;

            //parametry kvadratické rovnice
            double a = 1 + (xDy * xDy);
            double b = 2 * (xDy * cDy + s1.Y * xDy - s1.X);
            double c = s1.X * s1.X + cDy * cDy + 2 * s1.Y * cDy + s1.Y * s1.Y - r13s;

            double[] Xs = AnalyticGeometry.SolveQuadratic(a, b, c);
            double[] Ys = new double[Xs.Length];
            for (int i = 0; i < Xs.Length; i++)
            {
                Ys[i] = -(cDy + xDy * Xs[i]);
            }
            Sphere[] spheres = new SphereNode[2];
            spheres[0] = new Sphere(toPlace, Xs[0], Ys[0]);
            spheres[1] = new Sphere(toPlace, Xs[1], Ys[1]);
            return spheres;

            //#region collision check
            //bool [] collisions = {false,false};

            //foreach (Sphere  s in s1.Neighbours)
            //{
            //    collisions [0] = Collision(s,spheres [0]);
            //    collisions [1] = Collision(s,spheres [1]);

            //}
            //#endregion

            //#region boundaries
            //if (Xs.Length == 1)
            //{
            //    return new Sphere(toPlace, Xs[0], Ys[0]);
            //}

            //else if (Ys[0] < 0 || Xs[0] < 0)
            //{
            //    return new Sphere(toPlace, Xs[1], Ys[1]);
            //}
            //else if (Ys[1] < 0 || Xs[1] < 0)
            //{
            //    return new Sphere(toPlace, Xs[0], Ys[0]);
            //}
            //else if (Ys[0] < Ys[1])
            ////Choose lowest Y coordinate
            //{
            //    return new Sphere(toPlace, Xs[0], Ys[0]);
            //}
            //else 
            //{
            //    return new Sphere(toPlace,Xs[1], Ys[1]);
            //}
            //#endregion






        }

        /// <summary>
        /// Checks whether a Sphere is stable in its position
        /// </summary>
        /// <param name="toCheck">Sphere to be checked</param>
        /// <returns>true if the position is stable, false if it is not</returns>
        protected bool StabilityCheck(Sphere toCheck)
        {
            //todo: přetížit metodu, aby neighbours mohli být vstupním parametrem
            List<Sphere> neighbours = Collisions(toCheck);
            List<Vector> touchPoints = new List<Vector>(neighbours.Count);
            
            //?
            bool supportLeft = false;//todo: předělat na obecnější - vzít všechny body a hledat jestli je střed nad plochou, kterou vymezují
            bool supportRight = false;

            //opření o stěny
            if (toCheck.X-toCheck.Radius == 0)
            {
                supportLeft = true;
            }

            if (toCheck.X+toCheck.Radius == width)
            {
                supportRight = true;
            }

            //opření o koule
            for (int i = 0; i < neighbours.Count; i++)
			{
                if (!toCheck.Touch(neighbours[i]))
                    throw new Exception("Sphere in collision state - stability cannot be determined");
                else
                {
                    //todo: touchpoints se nemusí dávat do pole, stačí přímo zkontrolovat support
                    //todo: je potřeba kontrolovat jestli jsou touch points vzdáleny méně než poloměr koule (nepropadne)?
                    //determine the interseption point
                    touchPoints.Add(Sphere.TouchPoint(toCheck, neighbours[i]));
                    if (touchPoints[i].X < toCheck.X)
                        supportLeft = true;
                    if (touchPoints[i].X > toCheck.X)
                        supportRight = true;
                }		 
			}
            return supportRight && supportLeft;
        }
        
        #endregion
        #region container manipulation methods
        /// <summary>
        /// Empties the container (removes all spheres)
        /// </summary>
        public virtual void Clear()
        {
            //TODO: není nutné odstranit koule po jedné?
            this.spheres.Clear();
            this.statistics.Clear();
        }

        public abstract void Fill(SphereGenerator g);

        #endregion
        #region events


        /// <summary>
        /// Delegate for Changed Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="ce"></param>
        public delegate void ChangedEventHandler(object sender, ChangedEventArgs ce);

        /// <summary>
        /// Occurs when the Container is changed (eg. when a sphere is added or moved)
        /// </summary>
        public event ChangedEventHandler Changed;

        protected virtual void OnChanged(ChangedEventArgs ce)
        {
            // Make a temporary copy of the event to avoid possibility of
            // a race condition if the last subscriber unsubscribes
            // immediately after the null check and before the event is raised.
            ChangedEventHandler handler = Changed;
            if (handler != null)
            {
                handler(this,ce);
            }
        }

        /// <summary>
        /// Occurs when the Container has been filled 
        /// </summary>
        public event EventHandler Filled;

        protected virtual void OnFilled()
        {
            EventHandler handler = Filled;
            if (handler != null)
            {
                handler(this, null);
            }
        }


        /// <summary>
        /// Occurs when sphere is added
        /// </summary>
        public event EventHandler SphereAdded;
        protected virtual void OnSphereAdded()
        {
            EventHandler handler = SphereAdded;
            if (handler != null)
            {
                handler(this, null);
            }
        }

        public event EventHandler<SphereMovedEventArgs> SphereMoved;
        protected virtual void OnSphereMoved(Sphere s)
        {
            EventHandler<SphereMovedEventArgs> temp = SphereMoved;
            if (temp != null)
            {
                temp(this,new SphereMovedEventArgs(s));
            }
        }

        #endregion
        #region storing
        /// <summary>
        /// Saves to txt file
        /// </summary>
        /// <param name="fileName">path of file</param>
        public void SaveTxt(string fileName)
        {
            TextWriter tw = new StreamWriter(fileName);
            tw.WriteLine("šířka\t" + width.ToString());
            tw.WriteLine("výška\t" + height.ToString());
            foreach (Sphere item in this.spheres)
            {
                tw.WriteLine(item.ToString(true));
            }
            tw.Flush();
            tw.Close();
        }

        public void SvgExport(string file)
        {
            throw new NotImplementedException();
            //SvgNet.SvgElements.

        }

        public void ImageExport(string filename, ImageTypes type, ChangedEventArgs eventArgs)
        { }

        protected void Draw()
        {
        }

        public enum ImageTypes
        {
            emf, svg
        }
        #endregion

    }



    /// <summary>
    /// Event arguments for SphereContainer change
    /// </summary>
    public class ChangedEventArgs : EventArgs
    {
        public ChangedEventArgs(List<Sphere> added, List<Sphere> moved, List<Sphere> otherSpheres)
        {
            added = Added;
            moved = Moved;
            otherSpheres = OtherSpheres;

        }
        public List<Sphere> Added;
        public List<Sphere> Moved;
        public List<Sphere> OtherSpheres;
    }
}
