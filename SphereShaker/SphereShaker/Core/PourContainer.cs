﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SphereShaker.Core
{
    public class PourContainer : SphereContainer
    {
        #region constants
        private double stepDiffRatio = 0.075D;
        #endregion
        #region private fields
        /// <summary>
        /// Height of the highest sphere (y + r), no collisions possible above this point
        /// </summary>
        double topSphereHeight = 0;

        /// <summary>
        /// used for determinimg from where the sphere might fall
        /// </summary>
        Random randomGenerator = new Random();

        public SortedList<double, Sphere> lastLayer;

        /// <summary>
        /// number of spheres out of box - for determining fullness
        /// </summary>
        private int outOfBox = 0;

        /// <summary>
        /// how many balls placed out of box are tolereated before filling is terminated
        /// </summary>
        private int outOfBoxLimit = 300; //initial approximation, more precise number added later

        /// <summary>
        /// Maximum diameter of spheres possible in current generator
        /// </summary>
        private double maxDiameter = 0;
        #endregion
        public PourContainer(int width, int height)
            : base(width, height)
        {
            lastLayer = new SortedList<double, Sphere>();
        }

        public override void Clear()
        {
            base.Clear();
            outOfBox = 0;
            topSphereHeight = 0;
            lastLayer.Clear();
        }

        public override void Fill(SphereGenerator g)
        {
            if (g.FractionCount < 1)
            {
                throw new ArgumentException("Empty generator, nothing to fill with");
            }
            // precompute constants
            maxDiameter = g.Fractions[g.Fractions.Count - 1].Diameter;
            outOfBoxLimit = (int)Math.Ceiling(width / g.Smallest.Diameter * 5);
            double stepDifference = g.Smallest.Radius * stepDiffRatio;
            Vector gravityDrop = new Vector(0, -stepDifference, 0);

            for (int i = 0; i < 10000; i++)
            {
                SphereFraction sf = g.GetRandom();
                Sphere s = new Sphere(
                    sf,
                    randomGenerator.NextDouble() * (width - 2 * sf.Radius) + sf.Radius,
                    topSphereHeight + sf.Radius);
                Drop(s, gravityDrop);
                if (outOfBox >= outOfBoxLimit)
                {
                    break;
                }
            }


        }

        public override List<Sphere> Collisions(Sphere s)
        {
            return Sphere.Collisions(s, lastLayer.Values);
        }

        protected override bool Collides(Sphere toCheck)
        {
            foreach (Sphere si in lastLayer.Values)
            {
                if (Sphere.Collision(si, toCheck))
                    return true;

            }
            return false;
        }

        /// <summary>
        /// Moves the sphere by gravityDrop vector, until it is in a stable position
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        private bool Drop(Sphere s, Vector gravityDrop)
        {
            //spheres.Add(s);
            SphereState state;
            state.state = SphereStates.free;
            List<Sphere> col = Sphere.Collisions(s, lastLayer.Values);

            while (state.state != SphereStates.stable)
            {
                //assess current state, drop if free
                #region state

                //around walls
                if (s.X <= (0 + s.Radius) || s.X >= (width - s.Radius))
                {
                    if (s.X <= (0 + s.Radius))
                    {
                        Sphere.MoveToWall(s, 0, lastLayer.Values);
                    }
                    if (s.X >= (width - s.Radius))
                    {
                        Sphere.MoveToWall(s, width, lastLayer.Values);
                    }

                    if (StabilityCheck(s))
                    {
                        state.state = SphereStates.stable;
                        continue;
                    }
                    else
                    {
                        s.Position += gravityDrop;
                    }

                }


                col = Sphere.Collisions(s, lastLayer.Values);
                if (col.Count == 0)
                //no collision
                {
                    //no collision - is on ground?
                    if (s.Y <= s.Radius)
                    {
                        s.Y = s.Radius;//correction
                        //todo: korekce pozice k sousední kouli
                        state.state = SphereStates.stable;
                        continue;
                    }
                    else
                    {
                        //free => drop
                        s.Position += gravityDrop;
                        OnSphereMoved(s);
                        continue;
                    }

                }
                else
                {
                    //some collision
                    switch (col.Count)
                    {
                        case (1):
                            #region 1
                            //drop by možná mělo být až po posunu, ale toto bude asi rychlejší
                            s.Position += gravityDrop;

                            Sphere.MoveToSide(s, col[0]);
                            OnSphereMoved(s);
                            //tady zaznamenávat polohu
                            state.state = SphereStates.colliding;


                            if (s.Position.Y <= s.Radius)
                            {
                                s.Position.Y = s.Radius;
                                OnSphereMoved(s);
                                state.state = SphereStates.stable; //correction
                                //todo: tady znovu zkontrolovat kolize a případně posunout
                                continue;
                            }

                            break;
                            #endregion
                        case (2):
                            #region 2
                            {
                                List<Sphere> possibilities = base.PlaceToTouchTwo(s.Fraction, col[0], col[1]);
                                switch (possibilities.Count)
                                {
                                    //todo: udělat obecně
                                    case (0):
                                        throw new Exception("Something went wrong");
                                        break;
                                    case (1):
                                        OnSphereMoved(s);
                                        //je uprostřed, asi propadne
                                        //todo: 
                                        break;
                                    case (2):
                                        Vector[] dists = new Vector[2];
                                        //distances from original sphere - the smaller the better
                                        dists[0] = possibilities[0].Position - s.Position;
                                        dists[1] = possibilities[1].Position - s.Position;
                                        if (dists[0].Abs() < dists[1].Abs())
                                        {
                                            s = possibilities[0];

                                        }
                                        else
                                        {
                                            s = possibilities[1];
                                        }
                                        OnSphereMoved(s);

                                        //zjistit, jestli se zarovním na dotek s dvěma koulema nezačal dotýkat třetí - změna col.Count v průběhu
                                        List<Sphere> neighbours = Sphere.Collisions(s,lastLayer.Values);
                                        if (neighbours.Count != 2) // <2 =>nemůže být stabilní, >2 => neočekávané v tomto místě
                                        {
                                            break; //vyběhne z case 2, následuje break, znovu se spočtou sousedi a dostane se do case 3
                                        }

                                        if (StabilityCheck(s))
                                        {
                                            state.state = SphereStates.stable;
                                        }
                                        else
                                        {

                                            s.Position += gravityDrop;
                                            // zachytává se ke dvojici, když klesne, pořád koliduje s oběma, 
                                            // v dalším cyklu se počítá PlaceToTouch a zacyklí se - vyřešeno

                                            //výběr koule, která je níž (vedle té se má skulit)
                                            //Sphere toTouch = col[0].Y<col[1].Y ? col[0] : col[1];

                                            //verze 2 - výběr té, která je blíž v ose x
                                            Sphere toTouch = (Math.Abs(col[0].X - s.X) < Math.Abs(col[1].X - s.X)) ? col[0] : col[1];
                                            Sphere.MoveToSide(s, toTouch);
                                            OnSphereMoved(s);
                                        }
                                        break;

                                    default:
                                        throw new Exception("Something went wrong");
                                        break;
                                }

                                break;
                            }
                            #endregion
                        case (3):
                            #region 3
                            {
                                //todo: zvládl by tento kód i čtyři nebo více doteků?
                                //number of different pairs of colliding Spheres
                                int count = 0;
                                for (int i = 1; i < col.Count; i++)
                                {
                                    count += i;
                                }
                                List<Sphere> possibilities = new List<Sphere>();
                                //add pairs' solutions
                                for (int i = 0; i < col.Count - 1; i++)
                                {
                                    for (int j = i + 1; j < col.Count; j++)
                                    {

                                        //i...first sphere, j..second
                                        List<Sphere> p = PlaceToTouchTwo(s.Fraction, col[i], col[j]);
                                        //todo: upravit PlaceToTouchTwo na obecnější (kolikkoliv) + přepínač bližších variant
                                        Vector[] dists = new Vector[2];
                                        //distances from original sphere - the smaller the better
                                        dists[0] = p[0].Position - s.Position;
                                        dists[1] = p[1].Position - s.Position;
                                        if (dists[0].Abs() < dists[1].Abs())
                                        {
                                            possibilities.Add(p[0]);
                                        }
                                        else
                                        {
                                            possibilities.Add(p[1]);
                                        }
                                    }
                                }
                                List<Sphere> toRemove = new List<Sphere>();
                                foreach (Sphere pos in possibilities)
                                {
                                    //todo: hard collisions
                                    List<Sphere> colliders = Sphere.Collisions(pos,lastLayer.Values);
                                    foreach (Sphere c in colliders)
                                    {
                                        if (!Sphere.Touch(pos, c))
                                        {
                                            toRemove.Add(pos);
                                            break; //každou pos stačí odebrat jednou
                                        }
                                    }

                                }
                                foreach (Sphere tr in toRemove)
                                {
                                    possibilities.Remove(tr);
                                }

                                switch (possibilities.Count)
                                //možnosti umístění - některé budou nestabilní
                                //todo: je možné, aby nebyla žádná stabilní nebo jich bylo stabilních víc?
                                {

                                    case 0:
                                        throw new Exception("unexpected error");
                                        break;
                                    case 1: // jen jedna možnost, pokud je stabilní necháme být, jinak do toho drbnem                                   
                                        s = possibilities[0];
                                        if (StabilityCheck(s))
                                        {
                                            state.state = SphereStates.stable;
                                        }
                                        else
                                        {
                                            s.Position += gravityDrop;
                                            //todo: opětovné počítání neefektivní, upravit
                                            List<Sphere> sphereColliders = Collisions(s);

                                            //výběr koule, která je níž (vedle té se má skulit)
                                            //nejde debugovat
                                            //var toTouch = sphereColliders.Min<Sphere>((Sphere sph)=> {return sph.Y;});
                                            Sphere toTouch = sphereColliders[0];
                                            for (int i = 1; i < sphereColliders.Count; i++)
                                            {
                                                if (sphereColliders[i].Y < toTouch.Y)
                                                {
                                                    toTouch = sphereColliders[i];
                                                }
                                            }

                                            Sphere.MoveToSide(s, toTouch);
                                            OnSphereMoved(s);
                                        }
                                        break;

                                    default:
                                        //zjistíme první stabilní možnost.
                                        //todo: může být víc stabilních?
                                        for (int i = 0; i < possibilities.Count; i++)
                                        {
                                            if (StabilityCheck(possibilities[i]))
                                            {
                                                s = possibilities[i];
                                                state.state = SphereStates.stable;
                                                break;
                                            }
                                        }
                                        if (state.state != SphereStates.stable)
                                        {
                                            // kdyby bylo tady drbnutí tak by se tady zacyklovalo
                                            //ale stejně to musím zkusit, protože se sem občas dostane 

                                            s.Position += gravityDrop;
                                            //todo: opětovné počítání neefektivní, upravit
                                            List<Sphere> sphereColliders = Collisions(s);

                                            //výběr koule, která je níž (vedle té se má skulit)
                                            //nejde debugovat
                                            //var toTouch = sphereColliders.Min<Sphere>((Sphere sph)=> {return sph.Y;});
                                            Sphere toTouch = sphereColliders[0];
                                            for (int i = 1; i < sphereColliders.Count; i++)
                                            {
                                                if (sphereColliders[i].Y < toTouch.Y)
                                                {
                                                    toTouch = sphereColliders[i];
                                                }
                                            }

                                            Sphere.MoveToSide(s, toTouch);
                                            OnSphereMoved(s);

                                            //throw new Exception("unexpected error"); //nečekané množství možností
                                        }

                                        break;
                                }

                                break;
                            }
                            #endregion
                        default:
                            break;
                    }
                    continue;


                }
                #endregion
            }
            //kontrola jestli není mimmo hranice
            if (IsInContainer(s))
            {
                AddSphere(s);
                return true;
            }
            else
            {
                outOfBox++;
                return false;
            }

        }
        private bool IsInContainer(Sphere s)
        {
            return ((s.X - s.Radius >= 0) && (s.X + s.Radius <= width) && (s.Y - s.Radius >= 0) && (s.Y + s.Radius <= height));
        }

        private void AddSphere(Sphere s)
        {
            topSphereHeight = Math.Max(topSphereHeight, s.Y + s.Radius);
            spheres.Add(s);
            AddToLastLayer(s);
            #region add to stats
            if (!statistics.ContainsKey(s.Fraction))
            {
                statistics.Add(s.Fraction, 0);
                statistics[s.Fraction]++;
            }
            else
            {
                statistics[s.Fraction]++;
            }
            #endregion

            //TODO: neighbours
            OnSphereAdded();

            //todo: consider stable list for speed
            List<Sphere> Added = new List<Sphere>(1);
            Added.Add(s);
            OnChanged(new ChangedEventArgs(Added, null, null));
        }

        private void AddToLastLayer(Sphere s)
        {
            //projedeme poslední vrstvu a najdeme potencionálně zakryté koule
            double min = s.X - maxDiameter;
            double max = s.X + maxDiameter;
            List<Sphere> candidates = new List<Sphere>();

            double firstTouch = -1;
            double lastTouch = 0;
            foreach (var item in lastLayer)
            {
                
                // najdedme doteky
                // najdeme koule na vyřazení - jsou mezi prvním a posledním dotykem
                // todo: potencionální problémy u dotkyu se třemi
                if (item.Key >= min)
                {
                    if (Sphere.Touch(s, item.Value))
                    {
                        if (firstTouch<0)
                        {
                            firstTouch = item.Key;
                        }
                        lastTouch = item.Key;
                    }
                }
                if (item.Key>max)
                {
                    break;
                }
            }

            if (firstTouch > 0 & lastTouch!=firstTouch) //nalezeny min 2 doteky
            {
                int startRemove = lastLayer.IndexOfKey(firstTouch) + 1;
                int removeCnt = lastLayer.IndexOfKey(lastTouch) - startRemove;

                for (int i = 0; i < removeCnt; i++)
                {
                    lastLayer.RemoveAt(startRemove);
                }
            }


            //odstranit krajní kouli, pokud je přidsvaná na kraj
            if (s.X - s.Radius <= 0)
            {
                while (lastLayer.ElementAt(0).Value.X <= s.X)
                {
                    lastLayer.RemoveAt(0);
                }
            }
            if (s.X+s.Radius>=width)
            {
                while (lastLayer.ElementAt(lastLayer.Count-1).Value.X >= s.X)
                {
                    lastLayer.RemoveAt(lastLayer.Count-1);
                }   
            }
            if (lastLayer.ContainsKey(s.X))
            {
                lastLayer.Remove(s.X);
            }
            //přidáme novou
            lastLayer.Add(s.X, s);

        }
    }

    /// <summary>
    /// Placeholder for SphereState
    /// </summary>
    struct SphereState
    {
        /// <summary>
        /// State of sphere
        /// </summary>
        public SphereStates state;
        /// <summary>
        /// Spheres which collide
        /// </summary>
        public List<Sphere> colliders;

    }

    enum SphereStates
    {
        free, stable, colliding, touching, unknown
    }
}
