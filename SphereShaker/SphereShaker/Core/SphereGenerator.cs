﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SphereShaker.Core
{

    public class SphereGenerator
    {
        /// <summary>
        /// Gets the smallest fraction of the SphereGenerator
        /// </summary>
        public SphereFraction Smallest
        {
            get
            {
                if (spheres.Count < 1)
                {
                    return null;
                }
                return spheres[0];
            }
        }

        public void Add(SphereFraction fraction)
        {
            spheres.Add(fraction);
            fraction.RaduisChanged += new EventHandler(fraction_RaduisChanged);
            SortFractions();

        }

        void fraction_RaduisChanged(object sender, EventArgs e)
        {
            // todo: vyřešit
            // mělo by tady být, ale dělá problémy s UI - kolekce změní uvnitř foreach
            //SortFractions();
        }

        public SphereGenerator()
        {
            rand = new Random();
            spheres = new List<SphereFraction>(fractionsCnt);
        }

        public SphereGenerator(List<SphereFraction> fractions)
        {
            //TODO: consider better rnd
            rand = new Random();
            spheres = new List<SphereFraction>(fractionsCnt);
            spheres.AddRange(fractions);
            SortFractions();

            #region backup
            ////TODO: consider better rnd
            //rand = new Random();
            //fractions.Sort();

            //this.fractionsCnt = fractions.Count;
            //distribution = new List<double>(fractionsCnt);
            //spheres = new List<SphereFraction>(fractionsCnt);

            //double totalProbability = 0;
            //double totalProbability2 = 0;
            ////incase total probability is >1
            //for (int i = 0; i < fractionsCnt; i++)
            //{
            //    totalProbability += fractions[i].Probability;
            //}
            ////count frobability for each fraction
            //for (int i = 0; i < fractionsCnt; i++)
            //{
            //    totalProbability2 += fractions[i].Probability / totalProbability;
            //    distribution.Add(totalProbability2);
            //    spheres.Add(new SphereFraction(fractions[i].Radius));
            //}
            #endregion
        }
        private void SortFractions()
        {
            spheres.Sort();
            fractionsCnt = spheres.Count;

            //count distribution

            totalProbability = 0;
            distribution = new List<double>(fractionsCnt);
            for (int i = 0; i < fractionsCnt; i++)
            {
                totalProbability += spheres[i].ItemProbability;
                distribution.Add(totalProbability);
            }

        }
        private Random rand;
        private double totalProbability;
        private int fractionsCnt;
        private List<double> distribution;
        private List<SphereFraction> spheres;
        public List<SphereFraction> Fractions
        {
            get {
                SortFractions();
                return spheres; 
            }
        }

        private int last;
        public SphereFraction GetRandom()
        {
            double r = rand.NextDouble() * totalProbability;
            for (int i = 0; i < fractionsCnt; i++)
            {
                if (r < distribution[i])
                {
                    last = i;
                    return spheres[i];
                }
            }
            throw new Exception("Something went wrong");
        }

        public SphereFraction GetSmaller()
        {
            if (last == 0)
                return null;
            else
            {
                last--;
                return spheres[last];
            }

        }

        public void Remove()
        {
            if (spheres.Count > 0)
            {
                spheres.RemoveAt(spheres.Count - 1);
                SortFractions();
            }
        }

        public int FractionCount
        {
            get
            { return spheres.Count; }
        }
        public SphereGenerator(SphereGenerator copy)
        {
            if (copy != null)
            {

                rand = new Random();
                this.spheres = new List<SphereFraction>(copy.FractionCount);
                foreach (var item in copy.spheres)
                {
                    spheres.Add(item);
                    item.RaduisChanged += new EventHandler(fraction_RaduisChanged);
                }
                this.SortFractions();
            }
            else
            {
                rand = new Random();
                spheres = new List<SphereFraction>(fractionsCnt);
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder(this.FractionCount * 20);
            foreach (var item in spheres)
            {
                sb.Append(item.Diameter);
                sb.Append(";");
                sb.Append(item.VolumeProbability);
                sb.Append("|");
            }
            return sb.ToString();
        }
    }
}
