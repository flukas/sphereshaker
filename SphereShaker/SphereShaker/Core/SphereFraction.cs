﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SphereShaker.Core
{
    public class SphereFraction : IComparer<SphereFraction>, IComparable<SphereFraction>
    {
        public event EventHandler RaduisChanged;
        
        public double Diameter
        {
            get { return 2 * Radius; }
            set { Radius = value / 2; }
        }
        public SphereFraction(double radius)
        {
            Radius = radius;
            volumeProbability = 1;
        }
        public SphereFraction(double radius, double probability)
        {
            Radius = radius;
            volumeProbability = probability;
        }
        private double radius;
        public double Radius 
        {
            get
            {
                return radius;
            }

            set
            {
                radius = value;
                EventHandler temp = RaduisChanged;
                if (temp != null)
                {
                    temp(this,null);
                }
            }
        }


        public double ItemProbability
        {
            get
            {
                return volumeProbability / Radius / Radius / Radius;
            }
            set
            {
                volumeProbability = value * Radius * Radius * Radius;
            }
        }
        public double VolumeProbability
        {
            get
            {
                return volumeProbability;
            }
            set
            {
                volumeProbability = value;
            }
        }
        private double volumeProbability;
        public double Surface
        {
            get
            {
                return Math.PI * Radius * Radius;
            }
        }
        public double Perimeter
        {
            get
            {
                return 2 * Math.PI * Radius;
            }
        }
        //public System.Drawing.Color Color {get;set;}

        #region IComparer<SphereFraction> Members

        /// <summary>
        ///    Compares two objects and returns a value indicating whether one is less than,
        ///     equal to, or greater than the other.
        /// </summary>
        /// <param name="x">The first object to compare.</param>
        /// <param name="y">The second object to compare.</param>
        /// <returns>
        /// Value Condition Less than zerox is less than y.Zerox equals y.Greater than zerox is greater than y. 
        /// </returns>
        public int Compare(SphereFraction x, SphereFraction y)
        {
            return Math.Sign(x.Radius - y.Radius);
        }

        #endregion

        #region IComparable<SphereFraction> Members

        public int CompareTo(SphereFraction other)
        {
            return Math.Sign(this.Radius - other.Radius);
        }

        #endregion
        public override string ToString()
        {
            return string.Format("Radius: {0}, Probability {1}", Radius, VolumeProbability);
            //return base.ToString();
        }
    }

}
