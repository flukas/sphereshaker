﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SphereShaker.Core;

namespace SphereShaker.UI
{
    public partial class SphereVisualiser : UserControl
    {
        public SphereVisualiser()
        {
            InitializeComponent();
            //c = new SphereContainer(500, 300);
            
        }

        void c_Changed(object sender, ChangedEventArgs e)
        {
            this.Refresh();
            //System.Threading.Thread.Sleep(10);
        }

        #region display setup
        [
        Category("Objects to Paint"),
        Description("Specifies whether Centers of Spheres will be painted")
        ]
        public bool Centers { get; set; }
        
        [
        Category("Objects to Paint"),
        Description("Specifies whether Spheres will be painted")
        ]
        public bool Circles { get; set; }
        
        [
        Category("Objects to Paint"),
        Description("Specifies whether Statistics will be painted")
        ]
        public bool Statistics { get; set; }
        

        #endregion

        #region size
        protected const int sideBorder = 20;
        protected const int upDownBorder = 20;
        /// <summary>
        /// Constant for measure 
        /// </summary>
        protected double k;
        protected int contHeight;
        protected int contWidth;
        protected int vertOffset;
        protected int horizOffset;
        #endregion
        #region colors

        protected static readonly Color cContainerFill = Color.Gray;
        protected static readonly SolidBrush bContainerFill = new SolidBrush(cContainerFill);
        protected static readonly Color cContainerBorder = Color.Blue;
        protected static readonly Pen pContainerBorder = new Pen(cContainerBorder);
        protected static readonly Color cEllipse = Color.Black;
        protected static readonly Pen pEllipse = new Pen(cEllipse);

        protected static readonly Color cEllipseFill = Color.WhiteSmoke;
        protected static readonly Brush bEllipseFill = new SolidBrush(cEllipseFill);

        protected static readonly Color cCenters = Color.Red;
        protected static readonly Pen pCenters = new Pen(cCenters);

        protected static readonly Color cText = Color.Black;
        protected static readonly Brush bText = new SolidBrush(cText);
        protected static readonly Font fText = new Font("Arial", 8);

        #endregion
        #region data
        protected Core.SphereContainer c;

        public SphereContainer VisualisedContainer
        {
            get
            {
                return c;
            }
            set
            {
                c = value;
                if (c!=null)
                {
                    c.Changed += new SphereContainer.ChangedEventHandler(c_Changed); 
                    //c.SphereMoved += new EventHandler<SphereMovedEventArgs>(c_SphereMoved);
                }
                
               
            }
        }
        Sphere miscToPaint;
        void c_SphereMoved(object sender, SphereMovedEventArgs e)
        {
            miscToPaint = e.s;
            //System.Threading.Thread.Sleep(3);
            this.Refresh();
        }
        #endregion
        protected void ComputeConstants()
        {
            if (c==null)
            {
                return;
            }
            contWidth = this.Width - 2 * sideBorder;
            contHeight = this.Height - 2 * upDownBorder;
            double k1 = (double)contWidth / (double)c.Width;
            double k2 = (double)contHeight / (double)c.Height;
            k = Math.Min(k1, k2); //to fit different dimensions ratio
            contHeight = (int)Math.Round(k * (double)c.Height);
            contWidth = (int)Math.Round(k * (double)c.Width);
            vertOffset = (this.Width - contWidth) / 2;
            horizOffset = (this.Height - contHeight) / 2;
        }
        protected virtual void Draw(PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            //identification
            g.DrawString("Sphere Visualiser", fText, bText, new Point(10, 50));
            //TODO: move to change size event
            ComputeConstants();
            #region paint container
            g.FillRectangle(bContainerFill,
                vertOffset,
                horizOffset,
                contWidth,
                contHeight);
            g.DrawRectangle(pContainerBorder,
                vertOffset,
                horizOffset,
                contWidth,
                contHeight);
            #endregion
            #region paint spheres

            if (c==null)
            {
                return;
            }

            foreach (Sphere s in c.spheres)
            {
                
                //TODO: precompute center
                #region circles
                if (Circles)
                {
                    DrawSphere(s, g);
                }
                #endregion
                #region centers
                if (Centers)
                {
                    DrawCenter(s, g);
                }
                #endregion

            }
            #region Statistics
            if (Statistics)
            {
                g.DrawString("Objektů: " + c.Spheres, fText, bText, new Point(10, 10));
                g.DrawString("Zaplnění: " + c.FillFactor, fText, bText, new Point(10, 30));

            }
            #endregion

            if (false & miscToPaint != null )
            {
                g.DrawString("x: "+ miscToPaint.Position.X.ToString() + "y: " + miscToPaint.Position.Y.ToString(), fText, bText, new Point(10, 90));
                
                DrawSphere(miscToPaint, g,true);
            }
            

            #endregion


        }
        /// <summary>
        /// Draws a Sphere
        /// </summary>
        /// <param name="s">Sphere to be drawn</param>
        protected virtual void DrawSphere(Sphere s, Graphics g)
        { DrawSphere(s, g, false); }
        
        
        protected virtual void DrawSphere(Sphere s,Graphics g,bool markUp)

        {
            Pen xPen = pEllipse;
            if (markUp)
	{
                xPen.Color=Color.Red;
	}
            //fill
            g.FillEllipse(bEllipseFill,
                vertOffset + (float)((s.X - s.Radius) * k),
                Height - horizOffset - (float)((s.Y + s.Radius) * k),
                (float)((2 * s.Radius) * k),
                (float)((2 * s.Radius) * k));
            //border
            g.DrawEllipse(xPen,
                vertOffset + (float)((s.X - s.Radius) * k),
                Height - horizOffset - (float)((s.Y + s.Radius) * k),
                (float)((2 * s.Radius) * k),
                (float)((2 * s.Radius) * k));
        }

        protected virtual void DrawCenter(Sphere s, Graphics g)
        {
            g.DrawEllipse(pCenters,
               vertOffset + (float)((s.X) * k),
               Height - horizOffset - (float)((s.Y) * k),
               1,
               1);
        }

        protected PointF GetCenterPoint(Sphere s)
        {
            return new PointF(vertOffset + (float)((s.X) * k),
               Height - horizOffset - (float)((s.Y) * k));
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            Draw(e);

        }
    }
}
