﻿namespace SphereShaker.UI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.FractionSetup = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.ContainerSetup = new System.Windows.Forms.Button();
            this.visualisationSettingsUC1 = new SphereShaker.UI.VisualisationSettingsUC();
            this.sphereVisualiser1 = new SphereShaker.UI.SphereVisualiser();
            this.toolsControl1 = new SphereShaker.UI.ToolsControl();
            this.containerController1 = new SphereShaker.UI.ContainerController();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // FractionSetup
            // 
            this.FractionSetup.Location = new System.Drawing.Point(12, 177);
            this.FractionSetup.Name = "FractionSetup";
            this.FractionSetup.Size = new System.Drawing.Size(75, 23);
            this.FractionSetup.TabIndex = 4;
            this.FractionSetup.Text = "Frakce";
            this.FractionSetup.UseVisualStyleBackColor = true;
            this.FractionSetup.Click += new System.EventHandler(this.button1_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.ContainerSetup);
            this.splitContainer1.Panel1.Controls.Add(this.visualisationSettingsUC1);
            this.splitContainer1.Panel1.Controls.Add(this.toolsControl1);
            this.splitContainer1.Panel1.Controls.Add(this.FractionSetup);
            this.splitContainer1.Panel1.Controls.Add(this.containerController1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.sphereVisualiser1);
            this.splitContainer1.Size = new System.Drawing.Size(841, 620);
            this.splitContainer1.SplitterDistance = 234;
            this.splitContainer1.TabIndex = 5;
            // 
            // ContainerSetup
            // 
            this.ContainerSetup.Location = new System.Drawing.Point(94, 177);
            this.ContainerSetup.Name = "ContainerSetup";
            this.ContainerSetup.Size = new System.Drawing.Size(75, 23);
            this.ContainerSetup.TabIndex = 6;
            this.ContainerSetup.Text = "Nádoba";
            this.ContainerSetup.UseVisualStyleBackColor = true;
            this.ContainerSetup.Click += new System.EventHandler(this.ContainerSetup_Click);
            // 
            // visualisationSettingsUC1
            // 
            this.visualisationSettingsUC1.Location = new System.Drawing.Point(12, 315);
            this.visualisationSettingsUC1.Name = "visualisationSettingsUC1";
            this.visualisationSettingsUC1.Size = new System.Drawing.Size(150, 204);
            this.visualisationSettingsUC1.TabIndex = 5;
            this.visualisationSettingsUC1.Visualiser = this.sphereVisualiser1;
            this.visualisationSettingsUC1.Changed += new System.EventHandler(this.visualisationSettingsUC1_Changed);
            // 
            // sphereVisualiser1
            // 
            this.sphereVisualiser1.Centers = true;
            this.sphereVisualiser1.Circles = true;
            this.sphereVisualiser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sphereVisualiser1.Location = new System.Drawing.Point(0, 0);
            this.sphereVisualiser1.Name = "sphereVisualiser1";
            this.sphereVisualiser1.Size = new System.Drawing.Size(603, 620);
            this.sphereVisualiser1.Statistics = true;
            this.sphereVisualiser1.TabIndex = 0;
            this.sphereVisualiser1.VisualisedContainer = null;
            // 
            // toolsControl1
            // 
            this.toolsControl1.Location = new System.Drawing.Point(12, 206);
            this.toolsControl1.Name = "toolsControl1";
            this.toolsControl1.Size = new System.Drawing.Size(150, 103);
            this.toolsControl1.TabIndex = 2;
            this.toolsControl1.Create += new System.EventHandler(this.toolsControl1_Create);
            this.toolsControl1.Save += new System.EventHandler(this.toolsControl1_Save);
            // 
            // containerController1
            // 
            this.containerController1.Location = new System.Drawing.Point(12, 12);
            this.containerController1.Name = "containerController1";
            this.containerController1.Size = new System.Drawing.Size(204, 159);
            this.containerController1.TabIndex = 1;
            this.containerController1.Load += new System.EventHandler(this.containerController1_Load);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "Textový soubor|*.txt";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(841, 620);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private SphereShaker.UI.ContainerController containerController1;
        private SphereShaker.UI.ToolsControl toolsControl1;
        private System.Windows.Forms.Button FractionSetup;
        private SphereVisualiser sphereVisualiser1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private VisualisationSettingsUC visualisationSettingsUC1;
        private System.Windows.Forms.Button ContainerSetup;
    }
}

