﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SphereShaker.UI
{
    public partial class ToolsControl : UserControl
    {
        public ToolsControl()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Create(this, null);

        }

        public event EventHandler Create;

        public event EventHandler Save;

        private void buttonSave_Click(object sender, EventArgs e)
        {
            Save(this, null);
        }

    }
}
