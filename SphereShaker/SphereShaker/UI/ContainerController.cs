﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SphereShaker.Core;

namespace SphereShaker.UI
{
    public partial class ContainerController : UserControl
    {
        public ContainerController()
        {
            InitializeComponent();
            cont = new SphereShaker.Core.NodeContainer((int)nudWidth.Value,(int)nudHeight.Value);
        
        }
        private Core.SphereContainer cont;
        public Core.SphereContainer Cont {
            get { return cont; }
            set { cont = value;
            LoadFromCont();}
        }

        private void LoadFromCont()
        {
            nudWidth.Value = (decimal)cont.Width;
            nudHeight.Value = (decimal)cont.Height;
        }

        private void nudWidth_ValueChanged(object sender, EventArgs e)
        {
            cont.Width = (int)nudWidth.Value;
            if (OnContainerChanged != null)
            {
                OnContainerChanged(this, null);
            }
        }

        private void nudHeight_ValueChanged(object sender, EventArgs e)
        {
            cont.Height = (int)nudHeight.Value;
            if (OnContainerChanged!=null)
            {
                OnContainerChanged(this, null);
            }
        }

        public event EventHandler OnContainerChanged;

        private void rbPour_CheckedChanged(object sender, EventArgs e)
        {
            if (rbPour.Checked)
            {
                cont = new PourContainer((int)nudWidth.Value, (int)nudHeight.Value);
                EventHandler temp = OnContainerChanged;
                if (temp != null)
                {
                    temp(this,null);
                }
            }
            else
            {
                //cont = new NodeContainer((int)nudWidth.Value, (int)nudHeight.Value);
            }
        }

        private void rbNode_CheckedChanged(object sender, EventArgs e)
        {
            if (rbNode.Checked)
            {
                cont = new NodeContainer((int)nudWidth.Value, (int)nudHeight.Value);
                EventHandler temp = OnContainerChanged;
                if (temp != null)
                {
                    temp(this, null);
                }
            }
            else
            {
                //cont = new PourContainer((int)nudWidth.Value, (int)nudHeight.Value);
            }
        } 
    }
}
