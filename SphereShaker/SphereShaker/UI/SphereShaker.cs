﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SphereShaker.Core;
using SphereShaker.Utility;
using SphereShaker.UI;

namespace SphereShaker.UI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            sg = new SphereGenerator();
            sg.Add(new SphereFraction(10, 1)); //default fraction 
            containerController1.OnContainerChanged += new EventHandler(containerController1_OnContainerChanged);
        }
        #region private members
        SphereGenerator sg;
        SphereContainer sc;
        #endregion

        #region event subscriptions
        void containerController1_OnContainerChanged(object sender, EventArgs e)
        {
            sc = this.containerController1.Cont;
            sc.Clear();
            // assigning proper visualiser
            this.splitContainer1.Panel2.Controls.Remove(this.sphereVisualiser1);
            if (sc is NodeContainer)
            {
                sphereVisualiser1 = new NodeVisualiser();
            }
            if (sc is PourContainer)
            {
                sphereVisualiser1 = new PourVisualiser();
            }
            this.sphereVisualiser1.Centers = true;
            this.sphereVisualiser1.Circles = true;
            this.sphereVisualiser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sphereVisualiser1.Location = new System.Drawing.Point(0, 0);
            this.sphereVisualiser1.Statistics = true;
            this.sphereVisualiser1.TabIndex = 0;
            this.splitContainer1.Panel2.Controls.Add(this.sphereVisualiser1);
        }

        private void toolsControl1_Create(object sender, EventArgs e)
        {

            this.sphereVisualiser1.VisualisedContainer = this.containerController1.Cont;

            sc.Clear();
            sc.Fill(sg);
            
            
            //sphereVisualiser1.Invalidate();
            sphereVisualiser1.Refresh();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FractionSetup fs = new FractionSetup(sg);
            DialogResult dr= fs.ShowDialog();
            if (DialogResult.OK==dr)
            {
                sg = fs.Generator;
            }
            //FractionSetup fs = new FractionSetup(null);
            //fs.Fractions = fractions;
            //fs.ShowDialog();
            //fractions = fs.Fractions;          
        }
        
        private void toolsControl1_Save(object sender, EventArgs e)
        {
            DialogResult d =  this.saveFileDialog1.ShowDialog();
            if (d == DialogResult.OK)
            {
                this.containerController1.Cont.SaveTxt(this.saveFileDialog1.FileName);
            }

            
        }

        private void visualisationSettingsUC1_Changed(object sender, EventArgs e)
        {
            this.sphereVisualiser1.Refresh();
        }

        private void ContainerSetup_Click(object sender, EventArgs e)
        {

        }
        #endregion

        private void containerController1_Load(object sender, EventArgs e)
        {

        }






    }
}
