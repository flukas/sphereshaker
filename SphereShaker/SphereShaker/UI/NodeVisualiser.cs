﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SphereShaker.Core;


namespace SphereShaker.UI
{
    public partial class NodeVisualiser : SphereVisualiser
    {
        public NodeVisualiser()
        {
            InitializeComponent();
        }

        #region display setup
        [
        Category("Objects to Paint"),
        Description("Specifies whether Neighbours Centers will be connected with lines")
        ]
        public bool NeighbourLines { get; set; }

        [
        Category("Objects to Paint"),
        Description("Specifies whether objects will be connected to their parents")
        ]
        public bool ParentLines { get; set; }

        [
        Category("Objects to Paint"),
        Description("Specifies whether objects will be connected to their children")
        ]
        public bool ChildLines { get; set; }

        [
        Category("Objects to Paint"),
        Description("Specifies whether objects will be marked by their id numbers")
        ]
        public bool Numbers { get; set; }

        [
        Category("Objects to Paint"),
        Description("Specifies whether Children of Spheres will be painted")
        ]
        public bool Children { get; set; }
        #endregion
        #region colours
        static readonly Color cChildEllipse = Color.Red;
        static readonly Pen pChildEllipse = new Pen(cChildEllipse);

        static readonly Color cChildLines = Color.Green;
        static readonly Pen pChildLines = new Pen(cChildLines, 4);

        static readonly Color cParentLines = Color.Red;
        static readonly Pen pParentLines = new Pen(cParentLines, 2);

        static readonly Color cNeighbourLines = Color.RoyalBlue;
        static readonly Pen pNeighbourLines = new Pen(cNeighbourLines);

        #endregion

        protected override void Draw(PaintEventArgs e)
        {
            base.Draw(e);
            //base.panel1 -> protected?
            Graphics g = e.Graphics;
            //identification
            g.DrawString("Node Visualiser", fText, bText, new Point(10, 50));
            if (c==null)
            {
                return;
            }
            foreach (Sphere s in c.spheres)
            {
                #region numbers
                //if (Numbers)
                //{
                //    g.DrawString(s.index.ToString(), fText, bText,
                //       vertOffset + (float)((s.X + 1) * k),
                //       Height - horizOffset - (float)((s.Y - 1) * k));
                //}
                #endregion
                #region kiddielines
                //if (ChildLines)
                //{
                //    foreach (SphereNode child in s.Children)
                //    {
                //        g.DrawLine(pChildLines,
                //            vertOffset + (float)((s.X) * k),
                //            Height - horizOffset - (float)((s.Y) * k),
                //            vertOffset + (float)((child.X) * k),
                //            Height - horizOffset - (float)((child.Y) * k));
                //    }
                //}
                #endregion
                #region parent lines
                //if (ParentLines)
                //{
                //    if (s.Parent != null)
                //    {
                //        g.DrawLine(pParentLines,
                //            vertOffset + (float)((s.X) * k),
                //            Height - horizOffset - (float)((s.Y) * k),
                //            vertOffset + (float)((s.Parent.X) * k),
                //            Height - horizOffset - (float)((s.Parent.Y) * k));
                //    }
                //}
                #endregion
                #region neighbour lines
                //if (NeighbourLines)
                //{
                //    foreach (SphereNode n in s.Neighbours)
                //    {
                //        g.DrawLine(pNeighbourLines,
                //            vertOffset + (float)((s.X) * k),
                //            Height - horizOffset - (float)((s.Y) * k),
                //            vertOffset + (float)((n.X) * k),
                //            Height - horizOffset - (float)((n.Y) * k));
                //    }
                //}
                #endregion                
            }

        }
        protected void DrawChildren(SphereNode s, Graphics g)
        {
            foreach (var child in s.Children)
            {
                DrawSphere(child,g);
                DrawChildren(child,g);
            }
        }
        protected override void DrawSphere(Sphere s,Graphics g)
        {
            //if (Circles)
            {
                //Graphics g = this.CreateGraphics();
                g.DrawEllipse(pChildEllipse,
                    vertOffset + (float)((s.X - s.Radius) * k),
                    Height - horizOffset - (float)((s.Y + s.Radius) * k),
                    (float)((2 * s.Radius) * k),
                    (float)((2 * s.Radius) * k));
            }
        }

            //if (Children && c.spheres.Count > 0)
            //{
            //    //DrawSphere(c.spheres[0],e.Graphics);
            //    //TODO: make work again
            //    //DrawChildren(c.spheres[0],e.Graphics);

            //}

    }
}
