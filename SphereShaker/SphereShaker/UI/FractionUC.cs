﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SphereShaker.Core;

namespace SphereShaker.UI
{
    public partial class FractionUC : UserControl
    {
        /// <summary>
        /// Gets or sets Description of the control
        /// </summary>
        public string Title
        {
            get
            {
                return this.fractionLabel.Text;
            }
            set
            {
                this.fractionLabel.Text = value;
            }
        }
        public FractionUC(SphereFraction sf)
        {
            InitializeComponent();
            fraction = sf;
            ToUI();
        }

        public FractionUC()
        {
            InitializeComponent();
            fraction = new SphereFraction(5d, 1d);
            fraction.RaduisChanged += new EventHandler(fraction_RaduisChanged);
            ToUI();
        }

        void fraction_RaduisChanged(object sender, EventArgs e)
        {
            ToUI();
        }
        public SphereFraction Fraction
        {
            get { return fraction; }
            set
            {
                fraction = value;
                ToUI();
            }
        }
        private SphereFraction fraction;

        private void ToUI()
        {
            nudDiameter.Value = (decimal)fraction.Diameter;
            nudProbability.Value = (decimal)fraction.VolumeProbability;

        }

        private void nudDiameter_ValueChanged(object sender, EventArgs e)
        {
            fraction.Diameter = (double)nudDiameter.Value;
        }

        private void nudProbability_ValueChanged(object sender, EventArgs e)
        {
            fraction.VolumeProbability = (double)nudProbability.Value;
        }
    }
}
