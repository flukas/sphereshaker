﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SphereShaker.Utility
{
    public static class AnalyticGeometry
    {
        /// <summary>
        /// Solves qudratic equation in format a*x^2+b*x+c=0
        /// </summary>
        /// <param name="a">parameter for x^2</param>
        /// <param name="b">parameter for x</param>
        /// <param name="c">absolute member</param>
        /// <returns>Array of one or two solution</returns>
        public static double[] SolveQuadratic(double a, double b, double c)
        {
            double[] result;
            double discriminant = b * b - 4 * a * c;
            if (discriminant == 0)
            {
                result = new double[1];
                result[0] = -b / 2 / a;
                return result;
            }

            else if (discriminant > 0)
            {
                double sqrtd = Math.Sqrt(discriminant);
                result = new double[2];
                result[0] = (-b + sqrtd) / 2 / a;
                result[1] = (-b - sqrtd) / 2 / a;
                return result;
            }
            else
            {
                //todo: consider returning empty array
                throw new NotImplementedException("negative discriminant - no solution in real plane");
                //return new double [0];
            }

        }

    }
}
