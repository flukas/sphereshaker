﻿using SphereShaker.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System;

namespace TestProject
{
    
    
    /// <summary>
    ///This is a test class for SphereGeneratorTest and is intended
    ///to contain all SphereGeneratorTest Unit Tests
    ///</summary>
    [TestClass()]
    public class SphereGeneratorTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for SortFractions
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SphereShaker.exe")]
        public void SortFractionsTest()
        {
            SphereGenerator_Accessor target = new SphereGenerator_Accessor(); 
            target.SortFractions();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for GetSmaller
        ///</summary>
        [TestMethod()]
        public void GetSmallerTest()
        {
            SphereGenerator target = new SphereGenerator(); // TODO: Initialize to an appropriate value
            SphereFraction expected = null; // TODO: Initialize to an appropriate value
            SphereFraction actual;
            actual = target.GetSmaller();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetRandom
        ///</summary>
        [TestMethod()]
        public void GetRandomTest()
        {
            //int fractions = 3;
            //int iterations = 1000000;
            //double[] radiuses = { 10d, 20d, 30d };
            //double[] probabilities = { 1d, 1d, 1d };
            ////double[] expected = new double [fractions];
            //double volumeExpected = 4 * Math.PI / 3 * (1000 + 8000 + 27000) / (1 + 1 + 1);
            //Dictionary<SphereFraction, ulong> stats = new Dictionary<SphereFraction, ulong>(fractions);
            //SphereGenerator target = new SphereGenerator();
            //for (int i = 0; i < fractions; i++)
            //{
            //    SphereFraction sf = new SphereFraction(radiuses[i], probabilities[i]);
            //    target.Add(sf);
            //    stats.Add(sf, 0);
            //}

            //for (int i = 0; i < iterations; i++)
            //{
            //    stats[target.GetRandom()]++;
            //}
            //double actualVolume = 0;
            //foreach (var item in stats)
            //{
            //    actualVolume += 4 / 3 * Math.PI * Math.Pow(item.Key.Radius, 3) * item.Value;
            //}
            ////SphereFraction actual;
            ////actual = target.GetRandom();
            //Assert.AreEqual(volumeExpected, actualVolume);
            //Assert.Inconclusive("Verify the correctness of this test method.");


            int fractions = 3;
            int iterations = 1000000;
            double[] radiuses = { 1d, 2d, 3d };
            double[] probabilities = { 1d, 1d, 1d };
            //double[] expected = new double [fractions];
            double volumeExpected = 4 * Math.PI / 3 * (36) *iterations;
            Dictionary<SphereFraction, ulong> stats = new Dictionary<SphereFraction, ulong>(fractions);
            SphereGenerator target = new SphereGenerator();
            for (int i = 0; i < fractions; i++)
            {
                SphereFraction sf = new SphereFraction(radiuses[i], probabilities[i]);
                target.Add(sf);
                stats.Add(sf, 0);
            }

            for (int i = 0; i < iterations; i++)
            {
                stats[target.GetRandom()]++;
            }
            double actualVolume = 0;
            foreach (var item in stats)
            {
                actualVolume += 4d / 3d * Math.PI * Math.Pow(item.Key.Radius, 3) * item.Value;
            }
            //SphereFraction actual;
            //actual = target.GetRandom();
            Assert.AreEqual(volumeExpected, actualVolume,1e-5);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Add
        ///</summary>
        [TestMethod()]
        public void AddTest()
        {
            SphereGenerator target = new SphereGenerator(); // TODO: Initialize to an appropriate value
            SphereFraction fraction = null; // TODO: Initialize to an appropriate value
            target.Add(fraction);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for SphereGenerator Constructor
        ///</summary>
        [TestMethod()]
        public void SphereGeneratorConstructorTest1()
        {
            List<SphereFraction> fractions = null; // TODO: Initialize to an appropriate value
            SphereGenerator target = new SphereGenerator(fractions);
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for SphereGenerator Constructor
        ///</summary>
        [TestMethod()]
        public void SphereGeneratorConstructorTest()
        {
            SphereGenerator target = new SphereGenerator();
            
            //Assert.Inconclusive("TODO: Implement code to verify target");
        }
    }
}
