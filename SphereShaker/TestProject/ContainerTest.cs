﻿using SphereShaker.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace TestProject
{
    
    
    /// <summary>
    ///This is a test class for ContainerTest and is intended
    ///to contain all ContainerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ContainerTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Place
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SphereShaker.exe")]
        public void PlaceTest()
        {
            //TODO: upravit aby fungovalo i s polem
            //Container_Accessor target = new Container_Accessor(); 
            //double num = 20;
            //SphereFraction toPlace = new SphereFraction(num); 
            //Sphere s1 = new Sphere(toPlace,num,num); 
            //Sphere s2 = new Sphere(toPlace, num, 3 * num); 
            ////odvození str 8
            //Sphere expected = new Sphere(toPlace,num*(System.Math.Sqrt(3)+1),2*num);
            //Sphere actual;
            //actual = target.Place(toPlace, s1, s2);
            //Assert.AreEqual(expected.X, actual.X, 1e-9);//14->pass;15->fail
            //Assert.AreEqual(expected.Y, actual.Y, 1e-9);
            //Assert.AreEqual(expected.Radius, actual.Radius,1e-9);

        }
    }
}
